package cz.cvut.run.dummy;

/**
 * Created by michal.sklenar on 02/12/15.
 */
public abstract class SuperCalculatorClass {

    public abstract int getResult();

    public abstract void addToResult(int value);

    public int test()
    {
        return 3;
    }
}
