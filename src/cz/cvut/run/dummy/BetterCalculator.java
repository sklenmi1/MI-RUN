package cz.cvut.run.dummy;

import java.util.Enumeration;

/**
 * Created by Tadeas on 01.11.2015.
 */
public class BetterCalculator extends SuperCalculatorClass {

    private int result = 0;

    public BetterCalculator(int result, int badResult) {
        this.result = result;
    }

    public BetterCalculator() {
        int a = result;
    }

    public void addToResult(int value) {
        result += value;
    }

    public float add(float a, float b) {
        return a + b;
    }

    public int add(int a, int b) {
        return a + b;
    }

    public int getResult() {
        return result;
    }

    public boolean test(TestInterface1 a) {
        return  a.testf();
    }
}
