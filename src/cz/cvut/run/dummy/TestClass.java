package cz.cvut.run.dummy;

/**
 * Created by Tadeas on 20.12.2015.
 */
public class TestClass implements TestInterface1{

    public void doAllTests() {

        /* Local field assigning */
        int a = 5;
        double b = 4.9;
        long c = 3L;
        boolean d = true;
        float e = 3.9f;
        String f = "test";

        if (a != 5) {
            throw new RuntimeException("Local field assign failed (integer)");
        }

        if (b != 4.9) {
            throw new RuntimeException("Local field assign failed (double)");
        }

        if (c != 3L) {
            throw new RuntimeException("Local field assign failed (long)");
        }


        if (!d) {
            throw new RuntimeException("Local field assign failed (boolean)");
        }


        if (e != 3.9f) {
            throw new RuntimeException("Local field assign failed (float)");
        }

        if (!f.equals("test")) {
            throw new RuntimeException("Local field assign failed (String)");
        }

        /* Creating object, invokeing its methods, work with its fields */
        BetterCalculator calc = new BetterCalculator();
        int addResult = calc.add(83902, 12356);
        if (addResult != 96258) {
            throw new RuntimeException("Wrong result of add operation");
        }

        System.out.println("Result before 'addToResult' " + addResult);

        calc.addToResult(234571);
        int result = calc.getResult();

        if (result != 234571) {
            throw new RuntimeException("Wrong result of add operation");
        }

        System.out.println("Result after 'addToResult' " + result);

        /* Invoke method from super class */
        if (calc.test() != 3) {
            throw new RuntimeException("Wrong return value of super class method");
        }

        System.out.println("Invoke method from super class - " + calc.test());

        /* invoke interface method */
        System.out.println("Invoke interface method - " + calc.test(this));


        BetterCalculator[] array = new BetterCalculator[4];
        array[2] = new BetterCalculator(5, 1);

        System.out.println("Result from array[2] (BetterCalculator) - " + array[2].getResult());

        int[][] arr = new int[4][5];
        arr[2][3] = 24;
        arr[3] = new int[10];
        arr[3][9] = 99;
        int var = arr[2][3];
        int var1 = arr[3][9];

        if (var != 24 || var1 != 99) {
            throw new RuntimeException("Wrong values in array");
        }

        System.out.println("Array on [2][3] - " + var + ", on [3][9] - " + var1);

        /*String operations test */
        String test = "Test String number 1";
        String test2 = test + "- appended part";

        if (!test2.equals("Test String number 1- appended part")) {
            throw  new RuntimeException("Wrong result of append operation");
        }

        System.out.println(test2);

        String test3 = test2.substring(0, 4);

        if (!test3.equals("Test")) {
            throw new RuntimeException("Wrong result of substring operation");
        }
        System.out.println(test3);
        String replaceS = test3.replace('e', 'a');

        if (!replaceS.equals("Tast")) {
            throw new RuntimeException("Wrong result of replace operation");
        }

        System.out.println(replaceS);

        boolean cont = test2.contains("append");
        if (!cont) {
            throw new RuntimeException("Wrong result of contains operation");
        }

        System.out.println("Result of contains 'append' - " + cont);

        int indexOf = test2.indexOf("part");
        if (indexOf != 31) {
            throw new RuntimeException("Wrong result of indexof operation");
        }
        System.out.println("Index of part " + indexOf);

        if (!String.valueOf(24).equals("24")) {
            throw new RuntimeException("Wrong result of valueOf operation");
        }
        System.out.println("ValueOf 24 in String - \"" + String.valueOf(24) + "\"");

        /* Try/catch/finally test */
        try {
            System.out.println("try text");
            throw new NullPointerException("null pointer throw message");
        } catch (NullPointerException ex) {
            System.out.println("null pointer catch - " + ex.getMessage());
        } finally {
            System.out.println("finally text");
        }

        /* throw without handler test */
        throw new NullPointerException("Throw nullpointer ex without handler test");

    }

    @Override
    public boolean testf() {
        return true;
    }
}
