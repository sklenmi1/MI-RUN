package cz.cvut.run.util;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.GarbageCollector;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.heap.HeapArray;
import cz.cvut.run.vm.heap.HeapObject;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JReference;

import java.nio.ByteBuffer;

/**
 * Created by Tadeas on 12.12.2015.
 */
public class HeapUtil {

    private static Heap heap = Heap.getInstance();
    private static ByteBuffer byteBuffer;

    public static boolean isPrimitiveArray(JReference ref) {
        byteBuffer = heap.getByteBuffer();
        byteBuffer.position(ref.getReference());
        return byteBuffer.getInt() == HeapArray.PRIMITIVE_ARRAY_REF_TAG;
    }

    public static JInteger getArrayLength(JReference ref) {
        byteBuffer = heap.getByteBuffer();
        byteBuffer.position(ref.getReference() + 8);
//        try {
//            byteBuffer.position(ref.getReference() + 8);
//        }catch (IllegalArgumentException ex){
//            System.out.println("should not be here!");
//        }
        return new JInteger(byteBuffer.getInt());
    }

    public static boolean isArrayReference(JReference ref) {
        byteBuffer = heap.getByteBuffer();
        byteBuffer.position(ref.getReference());
        return byteBuffer.getInt() != HeapObject.OBJECT_REF_TAG;
    }

    public static boolean isObjectReference(JReference ref) {
        byteBuffer = heap.getByteBuffer();
        //byteBuffer.position(ref.getReference());
        try {
            byteBuffer.position(ref.getReference());
        }catch (IllegalArgumentException ex){
            System.out.println("should not be here!");
        }
        return byteBuffer.getInt() == HeapObject.OBJECT_REF_TAG;
    }

    public static int getArrayAType(JReference ref) {
        byteBuffer = heap.getByteBuffer();
        byteBuffer.position(ref.getReference() + 4);
        return byteBuffer.getInt();
    }

    public static JReference getJReferenceFromNativeString(String nativeString) {
        int nativeStringLenght = nativeString.length();
        char[] nativeStringChars = nativeString.toCharArray();
        JClass jClass = JVM.getInstance().getClass("java/lang/String");
        Heap heap = Heap.getInstance();
        JReference stringRef = heap.allocateObject(jClass);
        GarbageCollector.addNeededReference(stringRef);

        JReference charArray = heap.allocateArray(5, nativeStringLenght);

        heap.putField(stringRef, jClass.getFieldIndex("value", "[C"), charArray);

        for (int i = 0; i < nativeStringLenght; i++) {
            heap.putArrayField(charArray, i, new JInteger(nativeStringChars[i]));
        }
        return stringRef;
    }

    public static String getNativeStringFromJReference(JReference reference) {
        Heap heap = Heap.getInstance();
        JClass jClass = heap.getObjectClass(reference);
        JReference charArray = (JReference) heap.getField(reference, jClass.getFieldIndex("value", "[C"));
        int charArrayLength = HeapUtil.getArrayLength(charArray).getValue();

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < charArrayLength; i++) {
            builder.append((char) ((JInteger) heap.getArrayField(charArray, i)).getValue());
        }

        return builder.toString();
    }

}
