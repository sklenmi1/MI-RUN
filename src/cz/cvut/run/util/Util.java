package cz.cvut.run.util;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.vm.heap.HeapArray;
import cz.cvut.run.vm.heap.HeapObject;
import cz.cvut.run.vm.types.*;

/**
 * Created by Tadeas on 14.11.2015.
 */
public class Util {

    /**
     * inspired by - http://stackoverflow.com/questions/11087404/how-to-compute-the-number-of-arguments-from-a-method-descriptor
     */
    public static int getMethodArgumentCount(String desc) {
        int beginIndex = desc.indexOf('(') + 1;
        int endIndex = desc.lastIndexOf(')');
        String x0 = desc.substring(beginIndex, endIndex);//Extract the part related to the arguments
        String x1 = x0.replace("[", "");//remove the [ symbols for arrays to avoid confusion.
        String x2 = x1.replace(";", "; ");//add an extra space after each semicolon.
        String x3 = x2.replaceAll("L\\S+;", "L");//replace all the substrings starting with L, ending with ; and containing non whitespace characters inbetween with L.
        String x4 = x3.replace(" ", "");//remove the previously inserted spaces.
        return x4.length();//count the number of elements left.
    }


    public static int spaceNeededInHeap(JClass objectClass) {
        return HeapObject.HEADER_SIZE + 9 * objectClass.getFieldsCount();
    }

    public static int spaceNeedInHeapForArray(int atype, int size) {
        return HeapArray.HEADER_SIZE + getAtypeSize(atype) * size;
    }

    public static int spaceNeedInHeapForArray(int atype, int[] size) {
        return spaceNeedInHeapForArray(atype, getDimensSize(size));
    }

    public static int spaceNeedInHeapForArray(int size) {
        return HeapArray.HEADER_SIZE + 4 * size;
    }

    public static int spaceNeedInHeapForArray(int[] size) {
        return HeapArray.HEADER_SIZE + 4 * getDimensSize(size);
    }

    public static int getDimensSize(int[] dimensions) {
        int dimensSize = 1;
        for (int a = 0; a < dimensions.length; a++) {
            dimensSize *= dimensions[a];
        }
        return dimensSize;
    }

    public static int getAtypeSize(int atype) {
        switch (atype) {
            case 4:
            case 5:
            case 6:
            case 8:
            case 9:
            case 10:
                return 4;
            case 7:
            case 11:
                return 8;
        }
        return -1;
    }

    public static String parseClassName(String className) {
        return className.replace("[", "").replace(";", "");
    }

    public static int getAtype(String refClassName) {
        switch (refClassName) {
            case "B":
                return 10;
            case "C":
                return 10;
            case "D":
                return 7;
            case "F":
                return 6;
            case "I":
                return 10;
            case "J":
                return 11;
            case "S":
                return 10;
            case "Z":
                return 10;
        }
        return 0;
    }

    public static JType getDefaultValueForField(String descriptor) {
        switch (descriptor.substring(0, 1)) {
            case "B":
                return new JInteger(0);
            case "C":
                return new JInteger(0);
            case "D":
                return new JDouble(0);
            case "F":
                return new JFloat(0);
            case "I":
                return new JInteger(0);
            case "J":
                return new JLong(0);
            case "S":
                return new JInteger(0);
            case "Z":
                return new JInteger(0);
            case "L":
                return JReference.NULL;
            case "[":
                return JReference.NULL;
        }
        throw new RuntimeException("Invalid descriptor for default value!!");
    }
}
