package cz.cvut.run.loader;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.attribute.JAttribute;
import cz.cvut.run.structure.attribute.JCodeAttribute;
import cz.cvut.run.structure.attribute.JConstantValueAttribute;
import cz.cvut.run.structure.attribute.JLineNumberTableAttribute;
import cz.cvut.run.structure.cp.*;
import cz.cvut.run.structure.exception.JExceptionTableItem;
import cz.cvut.run.structure.field.JField;
import cz.cvut.run.structure.linenumber.JLineNumberTableItem;
import cz.cvut.run.structure.method.JMethod;
import cz.cvut.run.vm.JVM;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by Tadeas on 01.11.2015.
 */
public class JClassLoader {

    private JClass actualLoadingClass;

    public static String getRTJarLocation() {
        String value = System.getProperty("sun.boot.class.path");
        int endIndex = value.indexOf("rt.jar");

        if (endIndex == -1) {
            throw new RuntimeException("Could not locate rt.jar!");
        }
        String endWithRTJAR = value.substring(0, endIndex + 6);
        int lastSemicolon;

        if (System.getProperty("os.name").contains("Windows")) {
            lastSemicolon = endWithRTJAR.lastIndexOf(';');
        } else {
            lastSemicolon = endWithRTJAR.lastIndexOf(':');
        }

        return endWithRTJAR.substring(lastSemicolon + 1);
    }

    /**
     * @param path - path to compiled .class file
     */
    public JClass loadClass(String path, String className) {
        File inputFile = new File(path + className);
        DataInputStream dataInputStream = null;
        try {
            if (inputFile.exists()) {
                dataInputStream = new DataInputStream(new FileInputStream(inputFile));
            } else {
                JarFile jarFile = new JarFile(JClassLoader.getRTJarLocation());
                Enumeration<JarEntry> jarEntries = jarFile.entries();
                JarEntry jarEntry;
                while (jarEntries.hasMoreElements()) {
                    jarEntry = jarEntries.nextElement();
                    if (jarEntry.getName().equals(className)) {
                        dataInputStream = new DataInputStream(jarFile.getInputStream(jarEntry));
                        break;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            //this catch should be invoked because I manually check file existence
            throw new RuntimeException("Class file not found : " + path + " - " + className);
        } catch (IOException e) {
            throw new RuntimeException("IOException with JarFile : " + path + " - " + className);
        }

        if (dataInputStream == null) {
            throw new RuntimeException("Class file not found : " + path + " - " + className);
        }

        actualLoadingClass = new JClass();

        try {

            readMagicNumber(dataInputStream);

            readVersion(dataInputStream);

            readCP(dataInputStream);

            readClassAccessFlag(dataInputStream);

            readThisClassIndex(dataInputStream);

            readSuperClassIndex(dataInputStream);

            readInterfaces(dataInputStream);

            readFields(dataInputStream);

            readMethods(dataInputStream);

            readClassAttributes(dataInputStream);


        } catch (IOException ex) {
            throw new RuntimeException("Could not read properly .class file!");
        } finally {
            try {
                dataInputStream.close();
            } catch (IOException e) {
                //TODO
                //e.printStackTrace();
            }
        }

        return actualLoadingClass;
    }


    /**
     * ---------Read basic info from classfile---------
     */

    private void readMagicNumber(DataInputStream dataInputStream) throws IOException {
        int magicCode = dataInputStream.readInt();
        if (0xCAFEBABE != magicCode) {
            throw new RuntimeException("Invalid magic number of class file!");
        }
    }

    private void readVersion(DataInputStream dataInputStream) throws IOException {
        short majorVersion = dataInputStream.readShort();
        short minorVersion = dataInputStream.readShort();

        if (majorVersion != 0 || minorVersion != 51) {
            throw new RuntimeException("Invalid major or minor version of class file!");
        }
    }

    private void readClassAccessFlag(DataInputStream dataInputStream) throws IOException {
        short accessFlag = dataInputStream.readShort();

        if ((accessFlag & JClass.ACC_ABSTRACT) == JClass.ACC_ABSTRACT) {
            actualLoadingClass.setIsAbstract(true);
        }
        if ((accessFlag & JClass.ACC_PUBLIC) == JClass.ACC_PUBLIC) {
            actualLoadingClass.setIsPublic(true);
        }
        if ((accessFlag & JClass.ACC_ANNOTATION) == JClass.ACC_ANNOTATION) {
            actualLoadingClass.setIsAnnotation(true);
        }
        if ((accessFlag & JClass.ACC_FINAL) == JClass.ACC_FINAL) {
            actualLoadingClass.setIsFinal(true);
        }
        if ((accessFlag & JClass.ACC_INTERFACE) == JClass.ACC_INTERFACE) {
            actualLoadingClass.setIsInterface(true);
        }
        if ((accessFlag & JClass.ACC_ENUM) == JClass.ACC_ENUM) {
            actualLoadingClass.setIsEnum(true);
        }
        if ((accessFlag & JClass.ACC_SUPER) == JClass.ACC_SUPER) {
            actualLoadingClass.setIsSuper(true);
        }
        if ((accessFlag & JClass.ACC_SYNTHETIC) == JClass.ACC_SYNTHETIC) {
            actualLoadingClass.setIsSynthetic(true);
        }
    }

    private void readThisClassIndex(DataInputStream dataInputStream) throws IOException {
        short thisClassIndex = dataInputStream.readShort();
        actualLoadingClass.setThisClassIndex(thisClassIndex);
    }

    private void readSuperClassIndex(DataInputStream dataInputStream) throws IOException {
        short superClassIndex = dataInputStream.readShort();
        actualLoadingClass.setSuperClassIndex(superClassIndex);
    }

    private void readInterfaces(DataInputStream dataInputStream) throws IOException {
        short interfacesCount = dataInputStream.readShort();

        int interfaceIndex = -1;
        for (int i = 0; i < interfacesCount; i++) {
            interfaceIndex = dataInputStream.readShort();
            actualLoadingClass.addInterfaceCPIndex(interfaceIndex);
        }

    }


    /**
     * ---------Load constant pool---------
     */

    private void readCP(DataInputStream dataInputStream) throws IOException {
        short CPSize = dataInputStream.readShort();
        byte CPRecordType;
        CPRecord singleRecord;
        boolean writeForbiddenEntry;
        for (int index = 1; index < CPSize; index++) {
            writeForbiddenEntry = false;
            CPRecordType = dataInputStream.readByte();
            switch (CPRecordType) {
                case CPRecord.CONSTANT_Methodref:
                    singleRecord = readCPMethodRef(dataInputStream);
                    break;
                case CPRecord.CONSTANT_Class:
                    singleRecord = readCPClass(dataInputStream);
                    break;
                case CPRecord.CONSTANT_Fieldref:
                    singleRecord = readCPFieldRef(dataInputStream);
                    break;
                case CPRecord.CONSTANT_InterfaceMethodref:
                    singleRecord = readCPInterfaceMethodRef(dataInputStream);
                    break;
                case CPRecord.CONSTANT_String:
                    singleRecord = readCPString(dataInputStream);
                    break;
                case CPRecord.CONSTANT_Integer:
                    singleRecord = readCPInteger(dataInputStream);
                    break;
                case CPRecord.CONSTANT_Float:
                    singleRecord = readCPFloat(dataInputStream);
                    break;
                case CPRecord.CONSTANT_Long:
                    singleRecord = readCPLong(dataInputStream);
                    /**
                     * according to jvms7.pdf 8-byte constants take two entries in CP
                     */
                    writeForbiddenEntry = true;
                    break;
                case CPRecord.CONSTANT_Double:
                    singleRecord = readCPDouble(dataInputStream);
                    /**
                     * according to jvms7.pdf 8-byte constants take two entries in CP
                     */
                    writeForbiddenEntry = true;
                    break;
                case CPRecord.CONSTANT_NameAndType:
                    singleRecord = readCPNameAndType(dataInputStream);
                    break;
                case CPRecord.CONSTANT_Utf8:
                    singleRecord = readCPUtf8(dataInputStream);
                    break;
                case CPRecord.CONSTANT_MethodHandle:
                    singleRecord = readCPMethodHandle(dataInputStream);
                    break;
                case CPRecord.CONSTANT_MethodType:
                    singleRecord = readCPMethodType(dataInputStream);
                    break;
                case CPRecord.CONSTANT_InvokeDynamic:
                    singleRecord = readCPInvokeDynamic(dataInputStream);
                    break;
                default:
                    singleRecord = null;
                    break;
            }
            actualLoadingClass.addToConstantPool(singleRecord);
            if (writeForbiddenEntry) {
                actualLoadingClass.addToConstantPool(new CPForbidden());
                index++;
            }

        }
        if (JVM.FULL_LOG) {
            System.out.println((char) 27 + "[34m" + "Constant pool loading finished." + (char) 27 + "[0m");
            System.out.println("------------------");
        }
    }

    private CPRecord readCPUtf8(DataInputStream dataInputStream) throws IOException {
        short length = dataInputStream.readShort();
        byte[] bytes = new byte[length];
        int readLength = dataInputStream.read(bytes);

        if (length != readLength) {
            throw new RuntimeException("Could not read whole utf8 CP record!");
        }

        CPUTF8 object = new CPUTF8();
        object.setLength(length);
        object.setBytes(bytes);
        return object;
    }


    private CPRecord readCPInvokeDynamic(DataInputStream dataInputStream) throws IOException {
        short bootstrapMethodAttrIndex = dataInputStream.readShort();
        short nameAndTypeIndex = dataInputStream.readShort();
        CPInvokeDynamic object = new CPInvokeDynamic();
        object.setBootstrapMethodAttrIndex(bootstrapMethodAttrIndex);
        object.setNameAndTypeIndex(nameAndTypeIndex);
        return object;
    }

    private CPRecord readCPMethodType(DataInputStream dataInputStream) throws IOException {
        short descriptorIndex = dataInputStream.readShort();
        CPMethodType object = new CPMethodType();
        object.setDescriptorIndex(descriptorIndex);
        return object;
    }

    private CPRecord readCPMethodHandle(DataInputStream dataInputStream) throws IOException {
        byte referenceKind = dataInputStream.readByte();
        short referenceIndex = dataInputStream.readShort();
        CPMethodHandle object = new CPMethodHandle();
        object.setReferenceKind(referenceKind);
        object.setReferenceIndex(referenceIndex);
        return object;
    }

    private CPRecord readCPNameAndType(DataInputStream dataInputStream) throws IOException {
        short nameIndex = dataInputStream.readShort();
        short descriptorIndex = dataInputStream.readShort();
        CPNameAndType object = new CPNameAndType();
        object.setNameIndex(nameIndex);
        object.setDescriptorIndex(descriptorIndex);
        return object;
    }

    private CPRecord readCPDouble(DataInputStream dataInputStream) throws IOException {
        /*int highBytes = dataInputStream.readInt();
        int lowBytes = dataInputStream.readInt();*/
        double value = dataInputStream.readDouble();
        CPDouble object = new CPDouble();
        //object.setHighBytes(highBytes);
        //object.setLowBytes(lowBytes);
        object.setValue(value);
        return object;
    }

    private CPRecord readCPLong(DataInputStream dataInputStream) throws IOException {
        /*int highBytes = dataInputStream.readInt();
        int lowBytes = dataInputStream.readInt();*/
        long value = dataInputStream.readLong();
        CPLong object = new CPLong();
        //object.setHighBytes(highBytes);
        //object.setLowBytes(lowBytes);
        object.setValue(value);
        return object;
    }

    private CPRecord readCPFloat(DataInputStream dataInputStream) throws IOException {
        float value = dataInputStream.readFloat();
        CPFloat object = new CPFloat();
        object.setValue(value);
        return object;
    }

    private CPRecord readCPInteger(DataInputStream dataInputStream) throws IOException {
        int bytes = dataInputStream.readInt();
        CPInteger object = new CPInteger();
        object.setBytes(bytes);
        return object;
    }

    private CPRecord readCPString(DataInputStream dataInputStream) throws IOException {
        short stringIndex = dataInputStream.readShort();
        CPString object = new CPString();
        object.setStringIndex(stringIndex);
        return object;
    }

    private CPRecord readCPInterfaceMethodRef(DataInputStream dataInputStream) throws IOException {
        short classIndex = dataInputStream.readShort();
        short nameAndTypeIndex = dataInputStream.readShort();
        if (classIndex < 0 || nameAndTypeIndex < 0) {
            throw new RuntimeException("Invalid constant pool index!");
        }
        CPInterfaceMethodRef object = new CPInterfaceMethodRef();
        object.setClassIndex(classIndex);
        object.setNameAndTypeIndex(nameAndTypeIndex);
        return object;
    }

    private CPRecord readCPFieldRef(DataInputStream dataInputStream) throws IOException {
        short classIndex = dataInputStream.readShort();
        short nameAndTypeIndex = dataInputStream.readShort();
        if (classIndex < 0 || nameAndTypeIndex < 0) {
            throw new RuntimeException("Invalid constant pool index!");
        }
        CPFieldRef object = new CPFieldRef();
        object.setClassIndex(classIndex);
        object.setNameAndTypeIndex(nameAndTypeIndex);
        return object;
    }

    private CPRecord readCPClass(DataInputStream dataInputStream) throws IOException {
        short nameIndex = dataInputStream.readShort();
        CPClass object = new CPClass();
        object.setNameIndex(nameIndex);
        return object;
    }

    private CPMethodRef readCPMethodRef(DataInputStream dataInputStream) throws IOException {
        short classIndex = dataInputStream.readShort();
        short nameAndTypeIndex = dataInputStream.readShort();
        if (classIndex < 0 || nameAndTypeIndex < 0) {
            throw new RuntimeException("Invalid constant pool index!");
        }
        CPMethodRef object = new CPMethodRef();
        object.setClassIndex(classIndex);
        object.setNameAndTypeIndex(nameAndTypeIndex);
        return object;
    }


    /**
     * ---------Load methods---------
     */

    private void readMethods(DataInputStream dataInputStream) throws IOException {
        short methodsCount = dataInputStream.readShort();
        JMethod method;
        short accessFlag;
        short nameIndex;
        short descriptorIndex;

        for (int i = 0; i < methodsCount; i++) {
            method = new JMethod(actualLoadingClass);
            accessFlag = dataInputStream.readShort();
            nameIndex = dataInputStream.readShort();
            descriptorIndex = dataInputStream.readShort();

            setMethodAccessFlag(accessFlag, method);
            method.setNameIndex(nameIndex);
            method.setDescriptorIndex(descriptorIndex);

            readMethodAttributes(dataInputStream, method);
            actualLoadingClass.addMethod(method);
        }
    }

    private void setMethodAccessFlag(short accessFlag, JMethod method) {
        if ((accessFlag & JMethod.ACC_ABSTRACT) == JMethod.ACC_ABSTRACT) {
            method.setIsAbstract(true);
        }
        if ((accessFlag & JMethod.ACC_BRIDGE) == JMethod.ACC_BRIDGE) {
            method.setIsBridge(true);
        }
        if ((accessFlag & JMethod.ACC_FINAL) == JMethod.ACC_FINAL) {
            method.setIsFinal(true);
        }
        if ((accessFlag & JMethod.ACC_NATIVE) == JMethod.ACC_NATIVE) {
            method.setIsNative(true);
        }
        if ((accessFlag & JMethod.ACC_PRIVATE) == JMethod.ACC_PRIVATE) {
            method.setIsPrivate(true);
        }
        if ((accessFlag & JMethod.ACC_PROTECTED) == JMethod.ACC_PROTECTED) {
            method.setIsProtected(true);
        }
        if ((accessFlag & JMethod.ACC_PUBLIC) == JMethod.ACC_PUBLIC) {
            method.setIsPublic(true);
        }
        if ((accessFlag & JMethod.ACC_STATIC) == JMethod.ACC_STATIC) {
            method.setIsStatic(true);
        }
        if ((accessFlag & JMethod.ACC_STRICT) == JMethod.ACC_STRICT) {
            method.setIsStrict(true);
        }
        if ((accessFlag & JMethod.ACC_SYNCHRONIZED) == JMethod.ACC_SYNCHRONIZED) {
            method.setIsSynchronized(true);
        }
        if ((accessFlag & JMethod.ACC_SYNTHETIC) == JMethod.ACC_SYNTHETIC) {
            method.setIsSynthetic(true);
        }
        if ((accessFlag & JMethod.ACC_VARARGS) == JMethod.ACC_VARARGS) {
            method.setIsVarargs(true);
        }
    }


    /**
     * ---------Read fields---------
     */

    private void readFields(DataInputStream dataInputStream) throws IOException {
        short fieldsCount = dataInputStream.readShort();
        JField field;
        short accessFlag;
        short nameIndex;
        short descriptorIndex;


        for (int i = 0; i < fieldsCount; i++) {
            field = new JField();
            accessFlag = dataInputStream.readShort();
            nameIndex = dataInputStream.readShort();
            descriptorIndex = dataInputStream.readShort();

            setFieldAccessFlag(accessFlag, field);
            field.setNameIndex(nameIndex);
            field.setName(actualLoadingClass.getFromConstantPool(nameIndex).toString());
            field.setDescriptorIndex(descriptorIndex);
            field.setDescriptor(actualLoadingClass.getFromConstantPool(descriptorIndex).toString());

            readFieldAttributes(dataInputStream, field);
            actualLoadingClass.addField(field);
        }
    }

    private void setFieldAccessFlag(short accessFlag, JField field) {
        if ((accessFlag & JField.ACC_ENUM) == JField.ACC_ENUM) {
            field.setIsEnum(true);
        }
        if ((accessFlag & JField.ACC_FINAL) == JField.ACC_FINAL) {
            field.setIsFinal(true);
        }
        if ((accessFlag & JField.ACC_PRIVATE) == JField.ACC_PRIVATE) {
            field.setIsPrivate(true);
        }
        if ((accessFlag & JField.ACC_PROTECTED) == JField.ACC_PROTECTED) {
            field.setIsProtected(true);
        }
        if ((accessFlag & JField.ACC_PUBLIC) == JField.ACC_PUBLIC) {
            field.setIsPublic(true);
        }
        if ((accessFlag & JField.ACC_STATIC) == JField.ACC_STATIC) {
            field.setIsStatic(true);
        }
        if ((accessFlag & JField.ACC_SYNTHETIC) == JField.ACC_SYNTHETIC) {
            field.setIsSynthetic(true);
        }
        if ((accessFlag & JField.ACC_TRANSIENT) == JField.ACC_TRANSIENT) {
            field.setIsTransient(true);
        }
        if ((accessFlag & JField.ACC_VOLATILE) == JField.ACC_VOLATILE) {
            field.setIsVolatile(true);
        }
    }


    /**
     * ---------Read attributes---------
     */

    private void readClassAttributes(DataInputStream dataInputStream) throws IOException {
        short attributesCount = dataInputStream.readShort();

        for (int i = 0; i < attributesCount; i++) {
            JAttribute attribute = readAttribute(dataInputStream);
            if (attribute != null) {
                actualLoadingClass.addAttribute(attribute);
            }
        }
    }

    private void readFieldAttributes(DataInputStream dataInputStream, JField field) throws IOException {
        Short attributesCount = dataInputStream.readShort();

        for (int i = 0; i < attributesCount; i++) {
            JAttribute attribute = readAttribute(dataInputStream);
            if (attribute != null) {
                field.addAttribute(attribute);
            }
        }
    }

    private void readMethodAttributes(DataInputStream dataInputStream, JMethod method) throws IOException {
        Short attributesCount = dataInputStream.readShort();

        for (int i = 0; i < attributesCount; i++) {
            JAttribute attribute = readAttribute(dataInputStream);
            if (attribute != null) {
                if (attribute.getName().equals(JCodeAttribute.NAME)) {
                    method.setCodeAttribute((JCodeAttribute) attribute);
                } else {
                    method.addAttribute(attribute);
                }
            }
        }
    }

    private JAttribute readAttribute(DataInputStream dataInputStream) throws IOException {
        short nameIndex = dataInputStream.readShort();
        CPUTF8 name = (CPUTF8) actualLoadingClass.getFromConstantPool(nameIndex);

        switch (name.toString()) {
            case JConstantValueAttribute.NAME:
                return readConstantValueAttribute(dataInputStream);
            case JCodeAttribute.NAME:
                return readCodeAttribute(dataInputStream);
            case JLineNumberTableAttribute.NAME:
                return readLineNumberTableAttribute(dataInputStream);
            default:
                skipNonImplementedAttribute(dataInputStream);
                break;
        }
        return null;
    }

    private JAttribute readLineNumberTableAttribute(DataInputStream dataInputStream) throws IOException {
        int attributeLength = dataInputStream.readInt();
        JLineNumberTableAttribute attribute = new JLineNumberTableAttribute();
        short lineNumberTableLength = dataInputStream.readShort();

        if (lineNumberTableLength > 0) {
            attribute.setLineNumberTableLength(lineNumberTableLength);

            for (int i = 0; i < lineNumberTableLength; i++) {
                short startPc = dataInputStream.readShort();
                short lineNumber = dataInputStream.readShort();

                JLineNumberTableItem lnti = new JLineNumberTableItem();
                lnti.setStartPc(startPc);
                lnti.setLineNumber(lineNumber);
                attribute.addLineNumberTableItem(lnti, i);
            }
        }
        return attribute;
    }

    private JAttribute readCodeAttribute(DataInputStream dataInputStream) throws IOException {
        int attributeLength = dataInputStream.readInt();
        JCodeAttribute attribute = new JCodeAttribute();
        short maxStack = dataInputStream.readShort();
        short maxLocals = dataInputStream.readShort();
        int codeLength = dataInputStream.readInt();
        byte[] instructions = new byte[codeLength];
        dataInputStream.read(instructions);

        attribute.setMaxStack(maxStack);
        attribute.setMaxLocals(maxLocals);
        attribute.setCodeLength(codeLength);
        attribute.setInstructions(instructions);

        short exceptionTableLength = dataInputStream.readShort();
        if (exceptionTableLength > 0) {
            attribute.setExceptionTableLength(exceptionTableLength);

            for (int i = 0; i < exceptionTableLength; i++) {
                short startPc = dataInputStream.readShort();
                short endPc = dataInputStream.readShort();
                short handlerPc = dataInputStream.readShort();
                short catchType = dataInputStream.readShort();

                JExceptionTableItem eti = new JExceptionTableItem();
                eti.setStartPc(startPc);
                eti.setEndPc(endPc);
                eti.setHandlerPc(handlerPc);
                eti.setCatchType(catchType);

                attribute.addExceptionTableItem(eti, i);
            }
        }

        short attributesCount = dataInputStream.readShort();

        for (int i = 0; i < attributesCount; i++) {
            JAttribute attr = readAttribute(dataInputStream);
            if (attr != null) {
                attribute.addAttribute(attr);
            }
        }

        return attribute;
    }

    private JConstantValueAttribute readConstantValueAttribute(DataInputStream dataInputStream) throws IOException {
        int attributeLength = dataInputStream.readInt();
        short contantValueIndex = dataInputStream.readShort();
        JConstantValueAttribute attribute = new JConstantValueAttribute();
        attribute.setConstantValueIndex(contantValueIndex);
        return attribute;
    }

    private void skipNonImplementedAttribute(DataInputStream dataInputStream) throws IOException {
        int attributeLength = dataInputStream.readInt();
        byte[] dummyArray = new byte[attributeLength];
        dataInputStream.read(dummyArray);
    }
}
