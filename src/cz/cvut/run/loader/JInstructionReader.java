package cz.cvut.run.loader;

import cz.cvut.run.instructions.*;
import cz.cvut.run.vm.JExecutionFrame;

/**
 * Created by Tadeas on 04.11.2015.
 */
public class JInstructionReader {

    public static JInstruction readInstruction(JExecutionFrame frame) {
        int instructionOPCode = frame.getNextByte() & 0xFF;
        switch (instructionOPCode) {
            case JInstruction.ins_athrow:
                return new JAthrowInstruction(instructionOPCode);
            case JInstruction.ins_instance_of:
                return new JInstanceOfInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_checkcast:
                frame.getNextNBytes(2);
                return new JNopInstruction(instructionOPCode);
            case JInstruction.ins_new:
                return new JNewInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_newarray:
                return new JNewArrayInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_anewarray:
                return new JANewArrayInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_multianewarray:
                return new JMultiANewArrayInstruction(instructionOPCode, frame.getNextNBytes(3));
            case JInstruction.ins_nop:
                return new JNopInstruction(instructionOPCode);
            case JInstruction.ins_dup:
                return new JDupInstruction(instructionOPCode);
            case JInstruction.ins_dup_x1:
                return new JDupX1Instruction(instructionOPCode);
            case JInstruction.ins_invokevirtual:
                return new JInvokeVirtualInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_invokespecial:
                return new JInvokeSpecialInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_invokestatic:
                return new JInvokeStaticInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_invokeinterface:
                return new JInvokeInterfaceInstruction(instructionOPCode, frame.getNextNBytes(4));
            case JInstruction.ins_putfield:
                return new JPutFieldInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_getfield:
                return new JGetFieldInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_putstatic:
                return new JPutStaticInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_getstatic:
                return new JGetStaticInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_aload:
                return new JALoadInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_aload_0:
                return new JALoadXInstruction(instructionOPCode, 0);
            case JInstruction.ins_aload_1:
                return new JALoadXInstruction(instructionOPCode, 1);
            case JInstruction.ins_aload_2:
                return new JALoadXInstruction(instructionOPCode, 2);
            case JInstruction.ins_aload_3:
                return new JALoadXInstruction(instructionOPCode, 3);
            case JInstruction.ins_aconst_null:
                return new JAConstNullInstruction(instructionOPCode);
            case JInstruction.ins_astore:
                return new JAStoreInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_astore_0:
                return new JAStoreXInstruction(instructionOPCode, 0);
            case JInstruction.ins_astore_1:
                return new JAStoreXInstruction(instructionOPCode, 1);
            case JInstruction.ins_astore_2:
                return new JAStoreXInstruction(instructionOPCode, 2);
            case JInstruction.ins_astore_3:
                return new JAStoreXInstruction(instructionOPCode, 3);
            case JInstruction.ins_arraylength:
                return new JArrayLengthInstruction(instructionOPCode);
            case JInstruction.ins_aastore:
                return new JAAStoreInstruction(instructionOPCode);
            case JInstruction.ins_aaload:
                return new JAALoadInstruction(instructionOPCode);
            case JInstruction.ins_bastore:
                return new JIAStoreInstruction(instructionOPCode);
            case JInstruction.ins_baload:
                return new JIALoadInstruction(instructionOPCode);
            case JInstruction.ins_castore:
                return new JIAStoreInstruction(instructionOPCode);
            case JInstruction.ins_caload:
                return new JIALoadInstruction(instructionOPCode);
            case JInstruction.ins_iastore:
                return new JIAStoreInstruction(instructionOPCode);
            case JInstruction.ins_iaload:
                return new JIALoadInstruction(instructionOPCode);
            case JInstruction.ins_fastore:
                return new JFAStoreInstruction(instructionOPCode);
            case JInstruction.ins_faload:
                return new JFALoadInstruction(instructionOPCode);
            case JInstruction.ins_dastore:
                return new JDAStoreInstruction(instructionOPCode);
            case JInstruction.ins_daload:
                return new JDALoadInstruction(instructionOPCode);
            case JInstruction.ins_lastore:
                return new JLAStoreInstruction(instructionOPCode);
            case JInstruction.ins_laload:
                return new JLALoadInstruction(instructionOPCode);
            case JInstruction.ins_sastore:
                return new JIAStoreInstruction(instructionOPCode);
            case JInstruction.ins_saload:
                return new JIALoadInstruction(instructionOPCode);
            case JInstruction.ins_bipush:
                return new JBipushInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_sipush:
                return new JSipushInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_goto:
                return new JGotoInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_goto_w:
                return new JGotoWInstruction(instructionOPCode, frame.getNextNBytes(4));
            case JInstruction.ins_if_icmpge:
                return new JIfIcmpgeInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_if_acmpeq:
                return new JIfAcmpeqInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_if_acmpne:
                return new JIfAcmpneInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_if_icmpeq:
                return new JIfIcmpeqInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_if_icmpgt:
                return new JIfIcmpgtInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_if_icmple:
                return new JIfIcmpleInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_if_icmplt:
                return new JIfIcmpltInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_ifge:
                return new JIfGeInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_ifeq:
                return new JIfEqInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_ifle:
                return new JIfLeInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_iflt:
                return new JIfLtInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_ifne:
                return new JIfNeInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_ifnonnull:
                return new JIfNonNullInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_ifnull:
                return new JIfNullInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_ifgt:
                return new JIfGtInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_if_icmpne:
                return new JIfIcmpneInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_ldc:
                return new JLdcInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_ldc_w:
                return new JLdcWInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_ldc2_w:
                return new JLdc2WInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_iconst_m1:
                return new JIConstXInstruction(instructionOPCode, -1);
            case JInstruction.ins_iconst_0:
                return new JIConstXInstruction(instructionOPCode, 0);
            case JInstruction.ins_iconst_1:
                return new JIConstXInstruction(instructionOPCode, 1);
            case JInstruction.ins_iconst_2:
                return new JIConstXInstruction(instructionOPCode, 2);
            case JInstruction.ins_iconst_3:
                return new JIConstXInstruction(instructionOPCode, 3);
            case JInstruction.ins_iconst_4:
                return new JIConstXInstruction(instructionOPCode, 4);
            case JInstruction.ins_iconst_5:
                return new JIConstXInstruction(instructionOPCode, 5);
            case JInstruction.ins_istore:
                return new JIStoreInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_istore_0:
                return new JIStoreXInstruction(instructionOPCode, 0);
            case JInstruction.ins_istore_1:
                return new JIStoreXInstruction(instructionOPCode, 1);
            case JInstruction.ins_istore_2:
                return new JIStoreXInstruction(instructionOPCode, 2);
            case JInstruction.ins_istore_3:
                return new JIStoreXInstruction(instructionOPCode, 3);
            case JInstruction.ins_iload:
                return new JILoadInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_iload_0:
                return new JILoadXInstruction(instructionOPCode, 0);
            case JInstruction.ins_iload_1:
                return new JILoadXInstruction(instructionOPCode, 1);
            case JInstruction.ins_iload_2:
                return new JILoadXInstruction(instructionOPCode, 2);
            case JInstruction.ins_iload_3:
                return new JILoadXInstruction(instructionOPCode, 3);
            case JInstruction.ins_iinc:
                return new JIIncInstruction(instructionOPCode, frame.getNextNBytes(2));
            case JInstruction.ins_iadd:
                return new JIAddInstruction(instructionOPCode);
            case JInstruction.ins_isub:
                return new JISubInstruction(instructionOPCode);
            case JInstruction.ins_imul:
                return new JIMulInstruction(instructionOPCode);
            case JInstruction.ins_idiv:
                return new JIDivInstruction(instructionOPCode);
            case JInstruction.ins_irem:
                return new JIRemInstruction(instructionOPCode);
            case JInstruction.ins_ior:
                return new JIOrInstruction(instructionOPCode);
            case JInstruction.ins_iand:
                return new JIAndInstruction(instructionOPCode);
            case JInstruction.ins_ixor:
                return new JIXorInstruction(instructionOPCode);
            case JInstruction.ins_ineg:
                return new JINegInstruction(instructionOPCode);
            case JInstruction.ins_ishr:
                return new JIShrInstruction(instructionOPCode);
            case JInstruction.ins_ishl:
                return new JIShlInstruction(instructionOPCode);
            case JInstruction.ins_iushr:
                return new JIUshrInstruction(instructionOPCode);
            case JInstruction.ins_i2d:
                return new JI2dInstruction(instructionOPCode);
            case JInstruction.ins_i2f:
                return new JI2fInstruction(instructionOPCode);
            case JInstruction.ins_i2l:
                return new JI2lInstruction(instructionOPCode);
            case JInstruction.ins_i2s:
                return new JI2sInstruction(instructionOPCode);
            case JInstruction.ins_fconst_0:
                return new JFConstXInstruction(instructionOPCode, 0.0f);
            case JInstruction.ins_fconst_1:
                return new JFConstXInstruction(instructionOPCode, 1.0f);
            case JInstruction.ins_fconst_2:
                return new JFConstXInstruction(instructionOPCode, 2.0f);
            case JInstruction.ins_fstore:
                return new JFStoreInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_fstore_0:
                return new JFStoreXInstruction(instructionOPCode, 0);
            case JInstruction.ins_fstore_1:
                return new JFStoreXInstruction(instructionOPCode, 1);
            case JInstruction.ins_fstore_2:
                return new JFStoreXInstruction(instructionOPCode, 2);
            case JInstruction.ins_fstore_3:
                return new JFStoreXInstruction(instructionOPCode, 3);
            case JInstruction.ins_fload:
                return new JFLoadInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_fload_0:
                return new JFLoadXInstruction(instructionOPCode, 0);
            case JInstruction.ins_fload_1:
                return new JFLoadXInstruction(instructionOPCode, 1);
            case JInstruction.ins_fload_2:
                return new JFLoadXInstruction(instructionOPCode, 2);
            case JInstruction.ins_fload_3:
                return new JFLoadXInstruction(instructionOPCode, 3);
            case JInstruction.ins_fadd:
                return new JFAddInstruction(instructionOPCode);
            case JInstruction.ins_fsub:
                return new JFSubInstruction(instructionOPCode);
            case JInstruction.ins_fmul:
                return new JFMulInstruction(instructionOPCode);
            case JInstruction.ins_fdiv:
                return new JFDivInstruction(instructionOPCode);
            case JInstruction.ins_frem:
                return new JFRemInstruction(instructionOPCode);
            case JInstruction.ins_fneg:
                return new JFNegInstruction(instructionOPCode);
            case JInstruction.ins_fcmpg:
                return new JFCmpgInstruction(instructionOPCode);
            case JInstruction.ins_fcmpl:
                return new JFCmplInstruction(instructionOPCode);
            case JInstruction.ins_f2d:
                return new JF2dInstruction(instructionOPCode);
            case JInstruction.ins_f2i:
                return new JF2iInstruction(instructionOPCode);
            case JInstruction.ins_f2l:
                return new JF2lInstruction(instructionOPCode);
            case JInstruction.ins_lconst_0:
                return new JLConstXInstruction(instructionOPCode, 0L);
            case JInstruction.ins_lconst_1:
                return new JLConstXInstruction(instructionOPCode, 1L);
            case JInstruction.ins_lstore:
                return new JLStoreInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_lstore_0:
                return new JLStoreXInstruction(instructionOPCode, 0);
            case JInstruction.ins_lstore_1:
                return new JLStoreXInstruction(instructionOPCode, 1);
            case JInstruction.ins_lstore_2:
                return new JLStoreXInstruction(instructionOPCode, 2);
            case JInstruction.ins_lstore_3:
                return new JLStoreXInstruction(instructionOPCode, 3);
            case JInstruction.ins_lload:
                return new JLLoadInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_lload_0:
                return new JLLoadXInstruction(instructionOPCode, 0);
            case JInstruction.ins_lload_1:
                return new JLLoadXInstruction(instructionOPCode, 1);
            case JInstruction.ins_lload_2:
                return new JLLoadXInstruction(instructionOPCode, 2);
            case JInstruction.ins_lload_3:
                return new JLLoadXInstruction(instructionOPCode, 3);
            case JInstruction.ins_ladd:
                return new JLAddInstruction(instructionOPCode);
            case JInstruction.ins_lsub:
                return new JLSubInstruction(instructionOPCode);
            case JInstruction.ins_lmul:
                return new JLMulInstruction(instructionOPCode);
            case JInstruction.ins_ldiv:
                return new JLDivInstruction(instructionOPCode);
            case JInstruction.ins_lrem:
                return new JLRemInstruction(instructionOPCode);
            case JInstruction.ins_lneg:
                return new JLNegInstruction(instructionOPCode);
            case JInstruction.ins_lor:
                return new JLOrInstruction(instructionOPCode);
            case JInstruction.ins_land:
                return new JLAndInstruction(instructionOPCode);
            case JInstruction.ins_lxor:
                return new JLXorInstruction(instructionOPCode);
            case JInstruction.ins_lcmp:
                return new JLCmpInstruction(instructionOPCode);
            case JInstruction.ins_lshl:
                return new JLShlInstruction(instructionOPCode);
            case JInstruction.ins_lshr:
                return new JLShrInstruction(instructionOPCode);
            case JInstruction.ins_lushr:
                return new JLUShrInstruction(instructionOPCode);
            case JInstruction.ins_l2d:
                return new JL2dInstruction(instructionOPCode);
            case JInstruction.ins_l2f:
                return new JL2fInstruction(instructionOPCode);
            case JInstruction.ins_l2i:
                return new JL2iInstruction(instructionOPCode);
            case JInstruction.ins_dconst_0:
                return new JDConstXInstruction(instructionOPCode, 0.0);
            case JInstruction.ins_dconst_1:
                return new JDConstXInstruction(instructionOPCode, 1.0);
            case JInstruction.ins_dstore:
                return new JDStoreInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_dstore_0:
                return new JDStoreXInstruction(instructionOPCode, 0);
            case JInstruction.ins_dstore_1:
                return new JDStoreXInstruction(instructionOPCode, 1);
            case JInstruction.ins_dstore_2:
                return new JDStoreXInstruction(instructionOPCode, 2);
            case JInstruction.ins_dstore_3:
                return new JDStoreXInstruction(instructionOPCode, 3);
            case JInstruction.ins_dload:
                return new JDLoadInstruction(instructionOPCode, frame.getNextByte());
            case JInstruction.ins_dload_0:
                return new JDLoadXInstruction(instructionOPCode, 0);
            case JInstruction.ins_dload_1:
                return new JDLoadXInstruction(instructionOPCode, 1);
            case JInstruction.ins_dload_2:
                return new JDLoadXInstruction(instructionOPCode, 2);
            case JInstruction.ins_dload_3:
                return new JDLoadXInstruction(instructionOPCode, 3);
            case JInstruction.ins_dadd:
                return new JDAddInstruction(instructionOPCode);
            case JInstruction.ins_dsub:
                return new JDSubInstruction(instructionOPCode);
            case JInstruction.ins_dmul:
                return new JDMulInstruction(instructionOPCode);
            case JInstruction.ins_ddiv:
                return new JDDivInstruction(instructionOPCode);
            case JInstruction.ins_drem:
                return new JDRemInstruction(instructionOPCode);
            case JInstruction.ins_dneg:
                return new JDNegInstruction(instructionOPCode);
            case JInstruction.ins_dcmpg:
                return new JDCmpgInstruction(instructionOPCode);
            case JInstruction.ins_dcmpl:
                return new JDCmplInstruction(instructionOPCode);
            case JInstruction.ins_d2f:
                return new JD2fInstruction(instructionOPCode);
            case JInstruction.ins_d2i:
                return new JD2iInstruction(instructionOPCode);
            case JInstruction.ins_d2l:
                return new JD2lInstruction(instructionOPCode);
            case JInstruction.ins_return:
                return new JReturnInstruction(instructionOPCode);
            case JInstruction.ins_areturn:
                return new JAReturnInstruction(instructionOPCode);
            case JInstruction.ins_ireturn:
                return new JIReturnInstruction(instructionOPCode);
            case JInstruction.ins_freturn:
                return new JFReturnInstruction(instructionOPCode);
            case JInstruction.ins_lreturn:
                return new JLReturnInstruction(instructionOPCode);
            case JInstruction.ins_dreturn:
                return new JDReturnInstruction(instructionOPCode);
            case JInstruction.ins_pop:
                return new JPopInstruction(instructionOPCode);
            case JInstruction.ins_pop2:
                return new JPop2Instruction(instructionOPCode);
            case JInstruction.ins_i2c:
                //empty instruction, which doesn't perform any action
                return new JNopInstruction(instructionOPCode);
            case JInstruction.ins_i2b:
                //empty instruction, which doesn't perform any action
                return new JNopInstruction(instructionOPCode);
            case JInstruction.ins_monitorenter:
                return new JPopInstruction(instructionOPCode);
            case JInstruction.ins_monitorexit:
                return new JPopInstruction(instructionOPCode);
        }

        return null;
    }

}
