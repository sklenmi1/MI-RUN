package cz.cvut.run;

import cz.cvut.run.vm.JVM;

/**
 * Created by Tadeas on 01.11.2015.
 */
public class Main {

    public static void main(String[] args) {
        long timeMillis = System.currentTimeMillis();
        JVM jvm = JVM.getInstance();
//        args = new String[3];
//        args[0] = "D:/Dropbox (SKOUMAL, s.r.o.)/Skola/RUN/MI-RUN/data/cz/cvut/run/example/src/";
//        args[1] = "Main";
////        args[0] = "D:/Dropbox (SKOUMAL, s.r.o.)/Skola/RUN/MI-RUN/data/cz/cvut/run/dummy/";
////        args[1] = "TadeClass";
//        args[2] = "D:/Dropbox (SKOUMAL, s.r.o.)/Skola/RUN/MI-RUN/data/cz/cvut/run/example/data/test_instance.cnf";
        if (args.length < 2) {
            System.out.println("Wrong arguments! It's count should be at least 2.");
            System.out.println("First argument - path to a folder with main class.");
            System.out.println("Second argument - name of main class without extension!");
            System.out.println("Next arguments are arguments for a main method of main class.");
        } else {
            String[] arguments = new String[args.length - 2];
            for (int i = 2; i < args.length; i++) {
                arguments[i - 2] = args[i];
            }
            jvm.start(args[0], args[1] + ".class", arguments);
        }

        if (JVM.FULL_LOG) {
            System.out.println("Time - " + (System.currentTimeMillis() - timeMillis) + " ms");
        }
    }

}
