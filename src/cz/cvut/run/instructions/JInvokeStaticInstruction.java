package cz.cvut.run.instructions;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.cp.CPClass;
import cz.cvut.run.structure.cp.CPMethodRef;
import cz.cvut.run.structure.cp.CPNameAndType;
import cz.cvut.run.structure.method.JMethod;
import cz.cvut.run.util.Util;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JNativeExecutionFrame;
import cz.cvut.run.vm.JVM;

import java.nio.ByteBuffer;
import java.util.Stack;

/**
 * Created by Tadeas on 14.11.2015.
 */
public class JInvokeStaticInstruction extends JInstruction {

    private byte[] index;
    private String methodDescriptor;

    public JInvokeStaticInstruction(int opCode, byte[] index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame actualFrame = executionFrameStack.peek();
        JClass actualClass = actualFrame.getMethod().getJClass();
        CPMethodRef methodRef = (CPMethodRef) actualClass.getFromConstantPool(ByteBuffer.wrap(index).getShort());

        CPClass CPclass = (CPClass) actualClass.getFromConstantPool(methodRef.getClassIndex());
        String className = actualClass.getFromConstantPool(CPclass.getNameIndex()).toString();
        JClass refClass = JVM.getInstance().getClass(className);

        CPNameAndType methodNameAndType = (CPNameAndType) actualClass.getFromConstantPool(methodRef.getNameAndTypeIndex());
        String methodName = actualClass.getFromConstantPool(methodNameAndType.getNameIndex()).toString();
        String methodDescriptor = actualClass.getFromConstantPool(methodNameAndType.getDescriptorIndex()).toString();

        JMethod method = refClass.findMethod(methodName, methodDescriptor);

        if (method == null) {
            throw new RuntimeException("Trying to invoke method which not found!");
        }

        int argumentCount = Util.getMethodArgumentCount(methodDescriptor);

        /**
         * pop actual stack according to this website http://cs.au.dk/~mis/dOvs/jvmspec/ref--34.html
         */

        JExecutionFrame newFrame;

        if (!method.isNative()) {
            newFrame = new JExecutionFrame(method);
        } else {
            if (JVM.FULL_LOG) {
                System.out.println((char) 27 + "[35m" + "Invoke native method - " + methodName + (char) 27 + "[0m");
                System.out.println("------------------");
            }
            newFrame = new JNativeExecutionFrame(actualFrame, method);
        }

        executionFrameStack.push(newFrame);

        for (int i = argumentCount - 1; i >= 0; i--) {
            newFrame.addToLocalVariables(i, actualFrame.getOperandStack().pop());
        }

        if (JVM.FULL_LOG && !method.isNative()) {
            System.out.println("Call invokestatic instruction from method " + actualFrame.getMethod().getName() + " from class " + actualFrame.getMethod().getJClass().getClassName());
            System.out.println("Invokeing method " + methodName + " from class " + refClass.getClassName());
            System.out.println("------------------");
        }

    }
}
