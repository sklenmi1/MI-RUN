package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JReference;
import cz.cvut.run.vm.types.JType;

import java.util.Stack;

/**
 * Created by michal.sklenar on 12/12/15.
 */
public class JNewArrayInstruction extends JInstruction {

    private byte atype;

    public JNewArrayInstruction(int opCode, byte atype) {
        super(opCode);
        this.atype = atype;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        int count = ((JInteger) frame.getOperandStack().pop()).getValue();

        JReference reference = Heap.getInstance().allocateArray(atype, count);
        frame.getOperandStack().push(reference);

        if (JVM.FULL_LOG) {
            System.out.println("Call newarray instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
