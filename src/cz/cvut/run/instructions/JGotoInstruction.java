package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;

import java.nio.ByteBuffer;
import java.util.Stack;

/**
 * Created by Tadeas on 15.11.2015.
 */
public class JGotoInstruction extends JInstruction {
    private byte[] jumpIndex;

    public JGotoInstruction(int opCode, byte[] jumpIndex) {
        super(opCode);
        this.jumpIndex = jumpIndex;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        /**
         * according to JVM.pdf GOTO instruction jump index = index of goto instruction + goto offset,
         * but because we already read 1 byte for goto opcode + 2 bytes for goto jump index we have to
         * decrease index by 3
         */

        executionFrameStack.peek().addOffsetInProgramCounter(ByteBuffer.wrap(jumpIndex).getShort() - 3);

        if (JVM.FULL_LOG) {
            JExecutionFrame frame = executionFrameStack.peek();
            System.out.println("Call goto instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("------------------");
        }
    }
}
