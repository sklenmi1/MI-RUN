package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JFloat;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JFStoreXInstruction extends JInstruction {

    private int index;

    public JFStoreXInstruction(int opCode, int index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JFloat operand = (JFloat) frame.getOperandStack().pop();
        frame.addToLocalVariables(index, operand);

        if (JVM.FULL_LOG) {
            System.out.println("Call fstore_" + index + " instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On position " + index + " in local variables is: " + frame.getFromLocalVariables(index));
            System.out.println("------------------");
        }
    }
}
