package cz.cvut.run.instructions;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.cp.CPClass;
import cz.cvut.run.structure.cp.CPMethodRef;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JReference;

import java.nio.ByteBuffer;
import java.util.Stack;

/**
 * Created by michal.sklenar on 12/12/15.
 */
public class JANewArrayInstruction extends JInstruction {

    private byte [] index;

    public JANewArrayInstruction(int opCode, byte [] index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        int count = ((JInteger) frame.getOperandStack().pop()).getValue();

        JClass actualClass = frame.getMethod().getJClass();
        CPClass classRef = (CPClass) actualClass.getFromConstantPool(ByteBuffer.wrap(index).getShort());
        String className = actualClass.getFromConstantPool(classRef.getNameIndex()).toString();

        JClass refClass = JVM.getInstance().getClass(className);

        JReference reference = Heap.getInstance().allocateArray(refClass, count);

        frame.getOperandStack().push(reference);

        if (JVM.FULL_LOG) {
            System.out.println("Call anewarray instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
