package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JDouble;
import cz.cvut.run.vm.types.JInteger;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JDLoadInstruction extends JInstruction {

    private byte index;

    public JDLoadInstruction(int opCode, byte index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JDouble operand = (JDouble) frame.getFromLocalVariables(index & 0xFF);
        frame.getOperandStack().push(operand);

        if (JVM.FULL_LOG) {
            JExecutionFrame newFrame = executionFrameStack.peek();
            System.out.println("Call dload instruction from method " + newFrame.getMethod().getName() + " from class " + newFrame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + newFrame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
