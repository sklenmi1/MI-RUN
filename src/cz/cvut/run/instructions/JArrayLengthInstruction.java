package cz.cvut.run.instructions;

import cz.cvut.run.util.HeapUtil;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JReference;

import java.util.Stack;

/**
 * Created by michal.sklenar on 12/12/15.
 */
public class JArrayLengthInstruction extends JInstruction {

    public JArrayLengthInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JReference arrayRef = (JReference) frame.getOperandStack().pop();
        JInteger length = HeapUtil.getArrayLength(arrayRef);

        frame.getOperandStack().push(length);

        if (JVM.FULL_LOG) {
            System.out.println("Call arraylength instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
