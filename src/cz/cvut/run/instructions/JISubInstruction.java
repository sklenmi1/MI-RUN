package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JInteger;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JISubInstruction extends JInstruction {
    public JISubInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();

        JInteger op1 = (JInteger) frame.getOperandStack().pop();
        JInteger op2 = (JInteger) frame.getOperandStack().pop();

        frame.getOperandStack().push(op2.sub(op1));

        if (JVM.FULL_LOG) {
            System.out.println("Call isub instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
