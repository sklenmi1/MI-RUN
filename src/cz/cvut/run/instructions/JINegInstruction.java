package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JInteger;

import java.util.Stack;

/**
 * Created by Tadeas on 18.11.2015.
 */
public class JINegInstruction extends JInstruction {

    public JINegInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JInteger value = (JInteger) frame.getOperandStack().pop();
        frame.getOperandStack().push(value.negate());

        if (JVM.FULL_LOG) {
            System.out.println("Call ineg instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
