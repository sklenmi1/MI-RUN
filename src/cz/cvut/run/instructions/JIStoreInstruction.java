package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JLong;

import java.util.Stack;

/**
 * Created by Tadeas on 14.11.2015.
 */
public class JIStoreInstruction extends JInstruction {

    private byte index;

    public JIStoreInstruction(int opCode, byte index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JInteger operand = (JInteger) frame.getOperandStack().pop();
        frame.addToLocalVariables(index & 0xFF, operand);

        if (JVM.FULL_LOG) {
            System.out.println("Call istore instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On position " + (index & 0xFF) + " in local variables is: " + frame.getFromLocalVariables(index & 0xFF));
            System.out.println("------------------");
        }
    }
}
