package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JInteger;

import java.nio.ByteBuffer;
import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JSipushInstruction extends JInstruction{

    private byte[] value;

    public JSipushInstruction(int opCode, byte[] value) {
        super(opCode);
        this.value = value;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JInteger operand = new JInteger(ByteBuffer.wrap(value).getShort());
        executionFrameStack.peek().getOperandStack().push(operand);

        if (JVM.FULL_LOG) {
            JExecutionFrame frame = executionFrameStack.peek();
            System.out.println("Call sipush instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
