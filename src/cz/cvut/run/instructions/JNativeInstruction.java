package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JNativeInstructionSolver;
import cz.cvut.run.vm.heap.GarbageCollector;
import cz.cvut.run.vm.types.JType;

import java.util.Stack;

/**
 * Created by michal.sklenar on 18/11/15.
 */
public class JNativeInstruction extends JInstruction {
    JExecutionFrame resultFrame;


    public JNativeInstruction(JExecutionFrame resultFrame) {
        super(-1);
        this.resultFrame = resultFrame;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JType result = JNativeInstructionSolver.solve(executionFrameStack.pop());

        if ((resultFrame != null) && (result != null)) {
            resultFrame.getOperandStack().push(result);
        }

        //GarbageCollector.start();
    }
}
