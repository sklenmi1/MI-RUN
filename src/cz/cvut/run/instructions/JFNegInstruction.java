package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JDouble;
import cz.cvut.run.vm.types.JFloat;

import java.util.Stack;

/**
 * Created by Tadeas on 18.11.2015.
 */
public class JFNegInstruction extends JInstruction {

    public JFNegInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JFloat value = (JFloat) frame.getOperandStack().pop();
        frame.getOperandStack().push(value.negate());

        if (JVM.FULL_LOG) {
            System.out.println("Call fneg instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
