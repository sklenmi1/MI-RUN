package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JLong;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JLStoreXInstruction extends  JInstruction {
    private int index;

    public JLStoreXInstruction(int opCode, int index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JLong operand = (JLong) frame.getOperandStack().pop();
        frame.addToLocalVariables(index, operand);

        if (JVM.FULL_LOG) {
            System.out.println("Call lstore_" + index + " instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On position " + index + " in local variables is: " + frame.getFromLocalVariables(index));
            System.out.println("------------------");
        }
    }
}
