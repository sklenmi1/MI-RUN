package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JFloat;
import cz.cvut.run.vm.types.JInteger;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JFLoadInstruction extends JInstruction {

    private byte index;

    public JFLoadInstruction(int opCode, byte index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JFloat operand = (JFloat) frame.getFromLocalVariables(index & 0xFF);
        frame.getOperandStack().push(operand);

        if (JVM.FULL_LOG) {
            System.out.println("Call fload instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
