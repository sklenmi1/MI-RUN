package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JDouble;
import cz.cvut.run.vm.types.JLong;

import java.util.Stack;

/**
 * Created by Tadeas on 18.11.2015.
 */
public class JDNegInstruction extends JInstruction {

    public JDNegInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JDouble value = (JDouble) frame.getOperandStack().pop();
        frame.getOperandStack().push(value.negate());

        if (JVM.FULL_LOG) {
            System.out.println("Call dneg instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
