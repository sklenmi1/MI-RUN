package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;

import java.util.Stack;

/**
 * Created by Tadeas on 14.11.2015.
 */
public class JPop2Instruction extends JInstruction {

    public JPop2Instruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        if (frame.getOperandStack().peek().getBytesLength() == 8) {
            frame.getOperandStack().pop();
        } else {
            frame.getOperandStack().pop();
            frame.getOperandStack().pop();
        }

        if (JVM.FULL_LOG) {
            System.out.println("Call pop2 instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("------------------");
        }
    }
}
