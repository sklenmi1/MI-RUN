package cz.cvut.run.instructions;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.cp.*;
import cz.cvut.run.structure.field.JField;
import cz.cvut.run.util.HeapUtil;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JFloat;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JReference;
import cz.cvut.run.vm.types.JType;

import java.util.Stack;

/**
 * Created by Tadeas on 14.11.2015.
 */
public class JLdcInstruction extends JInstruction {
    private byte index;

    public JLdcInstruction(int opCode, byte index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JClass actualClass = executionFrameStack.peek().getMethod().getJClass();
        CPRecord value = actualClass.getFromConstantPool(index & 0xFF);
        JType pushedType = null;
        if (value.getTag() == CPRecord.CONSTANT_Integer) {
            pushedType = new JInteger(((CPInteger) value).getValue());
        }

        if (value.getTag() == CPRecord.CONSTANT_Float) {
            pushedType = new JFloat(((CPFloat) value).getValue());
        }

        if (value.getTag() == CPRecord.CONSTANT_String) {
            String nativeString = (actualClass.getFromConstantPool(((CPString) value).getStringIndex())).toString();

            pushedType = HeapUtil.getJReferenceFromNativeString(nativeString);
        }

        executionFrameStack.peek().getOperandStack().push(pushedType);

        if (JVM.FULL_LOG) {
            JExecutionFrame frame = executionFrameStack.peek();
            System.out.println("Call ldc instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("------------------");
        }
    }
}
