package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JLong;
import cz.cvut.run.vm.types.JReference;

import java.util.Stack;

/**
 * Created by Tadeas on 14.11.2015.
 */
public class JAStoreInstruction extends JInstruction {

    private byte index;

    public JAStoreInstruction(int opCode, byte index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JReference operand = (JReference) frame.getOperandStack().pop();
        frame.addToLocalVariables(index & 0xFF, operand);

        if (JVM.FULL_LOG) {
            System.out.println("Call astore instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On position " + (index & 0xFF) + " in local variables is: " + frame.getFromLocalVariables(index & 0xFF) + " with value: " + ((JReference)frame.getFromLocalVariables(index & 0xFF)).getReference());
            System.out.println("------------------");
        }
    }
}
