package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JType;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JILoadXInstruction extends JInstruction {

    private int index;

    public JILoadXInstruction(int opCode, int index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JInteger operand = (JInteger) frame.getFromLocalVariables(index);
        frame.getOperandStack().push(operand);

        if (JVM.FULL_LOG) {
            JExecutionFrame newFrame = executionFrameStack.peek();
            System.out.println("Call iload_" + index + " instruction from method " + newFrame.getMethod().getName() + " from class " + newFrame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + newFrame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
