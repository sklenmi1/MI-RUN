package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JType;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JIReturnInstruction extends JInstruction {
    public JIReturnInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame oldFrame = executionFrameStack.pop();

        JInteger returnValue = (JInteger) oldFrame.getOperandStack().pop();
        executionFrameStack.peek().getOperandStack().push(returnValue);

        if (JVM.FULL_LOG) {
            JExecutionFrame newFrame = executionFrameStack.peek();
            System.out.println("Call ireturn instruction from method " + newFrame.getMethod().getName() + " from class " + newFrame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + newFrame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
