package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JInteger;

import java.nio.ByteBuffer;
import java.util.Stack;

/**
 * Created by Tadeas on 15.11.2015.
 */
public class JIfLeInstruction extends JInstruction {
    private byte[] jumpIndex;

    public JIfLeInstruction(int opCode, byte[] jumpIndex) {
        super(opCode);
        this.jumpIndex = jumpIndex;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame actualFrame = executionFrameStack.peek();
        JInteger firstInteger = (JInteger) actualFrame.getOperandStack().pop();

        if (firstInteger.getValue() <= 0) {

            /**
             * according to JVM.pdf IF instruction jump index = index of IF instruction + IF offset,
             * but because we already read 1 byte for IF opcode + 2 bytes for IF jump index we have to
             * decrease index by 3
             */

            actualFrame.addOffsetInProgramCounter(ByteBuffer.wrap(jumpIndex).getShort() - 3);
        }

        if (JVM.FULL_LOG) {
            System.out.println("Call ifle instruction from method " + actualFrame.getMethod().getName() + " from class " + actualFrame.getMethod().getJClass().getClassName());
            System.out.println("------------------");
        }
    }
}
