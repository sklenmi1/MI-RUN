package cz.cvut.run.instructions;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.attribute.JAttribute;
import cz.cvut.run.structure.attribute.JCodeAttribute;
import cz.cvut.run.structure.attribute.JLineNumberTableAttribute;
import cz.cvut.run.structure.exception.JExceptionTableItem;
import cz.cvut.run.structure.field.JField;
import cz.cvut.run.structure.linenumber.JLineNumberTableItem;
import cz.cvut.run.util.HeapUtil;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.exception.JVMException;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JReference;
import cz.cvut.run.vm.types.JType;

import java.io.IOException;
import java.util.Stack;

/**
 * Created by michal.sklenar on 11/12/15.
 */
public class JAthrowInstruction extends JInstruction {
    public JAthrowInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        String className = frame.getMethod().getJClass().getClassName();

        JReference objectRef = (JReference) frame.getOperandStack().pop();

        if (objectRef == null) {
            throw new NullPointerException("ObjectRef v instrukci athrow je null");
        }

        JClass throwableClass = JVM.getInstance().getClass("java/lang/Throwable");
        JClass refClass = Heap.getInstance().getObjectClass(objectRef);

        if (!refClass.isInstanceOf(throwableClass)) {
            throw new RuntimeException("Exception není instancí, nebo potomkem Throwable.");
        }

        while (!executionFrameStack.isEmpty()) {
            frame = executionFrameStack.peek();

            for (JExceptionTableItem eti : frame.getMethod().getCodeAttribute().getExceptionTable()) {
                int invokePc = frame.getProgramCounter() - 1;

                if (eti.getStartPc() <= invokePc && (eti.getEndPc() - 1) >= invokePc) {
                    JClass actualClass = frame.getMethod().getJClass();
                    JClass catchClass = eti.getCatchTypeClass(actualClass);

                    if ((catchClass != null && refClass.isInstanceOf(catchClass)) || eti.getCatchType() == 0) {
                        frame.setProgramCounter(eti.getHandlerPc());
                        frame.getOperandStack().push(objectRef);
                        return;
                    }
                }
            }

            executionFrameStack.pop();
        }

        int lineNumber = frame.getMethod().getCodeAttribute().getLineNumber(frame.getProgramCounter());
        String errorMsg = "";
        int fieldIndex = refClass.getFieldIndex("detailMessage", "Ljava/lang/String;");

        JReference msgField = (JReference) Heap.getInstance().getField(objectRef, fieldIndex);
        if (msgField.getReference() >= 0) {
            errorMsg = HeapUtil.getNativeStringFromJReference(msgField);
        }

        errorMsg = errorMsg.length() == 0 ? "No exception handler was found for." : errorMsg;
        String message = "was thrown in class " + className + " at line number " + lineNumber + " with message : '" + errorMsg + "'";

        switch (refClass.getClassName()) {
            case "java/lang/RuntimeException":
                throw new RuntimeException("Exception " + message);
            case "java/lang/NullPointerException":
                throw new NullPointerException("Exception " + message);
            case "java/lang/IndexOutOfBoundsException":
                throw new IndexOutOfBoundsException("Exception " + message);
            default:
                throw new JVMException("Exception " + refClass.getClassName() + " " + message);
        }
    }
}
