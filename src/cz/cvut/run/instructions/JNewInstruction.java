package cz.cvut.run.instructions;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.cp.CPClass;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JReference;

import java.nio.ByteBuffer;
import java.util.Stack;

/**
 * Created by Tadeas on 18.11.2015.
 */
public class JNewInstruction extends JInstruction {

    private byte[] CPclassRef;

    public JNewInstruction(int opCode, byte[] CPclassRef) {
        super(opCode);
        this.CPclassRef = CPclassRef;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JClass actualJClass = executionFrameStack.peek().getMethod().getJClass();
        CPClass methodCPClassName = (CPClass) actualJClass.getFromConstantPool(ByteBuffer.wrap(CPclassRef).getShort());
        String methodClassName = actualJClass.getFromConstantPool(methodCPClassName.getNameIndex()).toString();
        JClass methodClass = JVM.getInstance().getClass(methodClassName);
        JReference ref = Heap.getInstance().allocateObject(methodClass);

        executionFrameStack.peek().getOperandStack().push(ref);

        if (JVM.FULL_LOG) {
            JExecutionFrame frame = executionFrameStack.peek();
            System.out.println("Call new instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
