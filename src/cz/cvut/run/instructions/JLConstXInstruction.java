package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JDouble;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JLong;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JLConstXInstruction extends JInstruction{

    private long value;

    public JLConstXInstruction(int opCode, long value) {
        super(opCode);
        this.value = value;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JLong operand = new JLong(value);
        executionFrameStack.peek().getOperandStack().push(operand);

        if (JVM.FULL_LOG) {
            JExecutionFrame frame = executionFrameStack.peek();
            System.out.println("Call lconst_" + value + " instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
