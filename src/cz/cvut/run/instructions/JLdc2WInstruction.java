package cz.cvut.run.instructions;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.cp.*;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.*;

import java.nio.ByteBuffer;
import java.util.Stack;

/**
 * Created by michal.sklenar on 18.11.2015.
 */
public class JLdc2WInstruction extends JInstruction {
    private byte[] index;

    public JLdc2WInstruction(int opCode, byte[] index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JClass actualClass = executionFrameStack.peek().getMethod().getJClass();
        CPRecord value = actualClass.getFromConstantPool(ByteBuffer.wrap(index).getShort());

        JType pushedType = null;
        if (value.getTag() == CPRecord.CONSTANT_Double) {
            pushedType = new JDouble(((CPDouble) value).getValue());
        }

        if (value.getTag() == CPRecord.CONSTANT_Long) {
            pushedType = new JLong(((CPLong) value).getValue());
        }

        executionFrameStack.peek().getOperandStack().push(pushedType);

        if (JVM.FULL_LOG) {
            JExecutionFrame frame = executionFrameStack.peek();
            System.out.println("Call ldc2_w instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("------------------");
        }
    }
}
