package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JLong;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JLXorInstruction extends JInstruction {

    public JLXorInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JLong op1 = (JLong) frame.getOperandStack().pop();
        JLong op2 = (JLong) frame.getOperandStack().pop();

        frame.getOperandStack().push(op2.bitwiseXor(op1));

        if (JVM.FULL_LOG) {
            System.out.println("Call lxor instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
