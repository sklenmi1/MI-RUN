package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JInteger;

import java.util.Stack;

/**
 * Created by michal.sklenar on 18.11.2015.
 */
public class JIIncInstruction extends JInstruction {
    private byte[] index;

    public JIIncInstruction(int opCode, byte[] index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();

        JInteger localVar = (JInteger) frame.getFromLocalVariables(index[0] & 0xFF);
        frame.addToLocalVariables(index[0] & 0xFF, localVar.add(new JInteger(index[1])));

        if (JVM.FULL_LOG) {
            System.out.println("Call iinc instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On position " + (index[0] & 0xFF) + " in local variables is: " + frame.getFromLocalVariables(index[0] & 0xFF));
            System.out.println("------------------");
        }
    }
}
