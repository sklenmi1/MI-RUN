package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JDouble;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JReference;

import java.util.Stack;

/**
 * Created by Tadeas on 14.11.2015.
 */
public class JAAStoreInstruction extends JInstruction {

    public JAAStoreInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();

        JReference value = (JReference) frame.getOperandStack().pop();
        JInteger index = (JInteger) frame.getOperandStack().pop();
        JReference arrayRef = (JReference) frame.getOperandStack().pop();

        Heap.getInstance().putArrayField(arrayRef, index.getValue(), value);

        if (JVM.FULL_LOG) {
            System.out.println("Call aastore instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("------------------");
        }
    }
}
