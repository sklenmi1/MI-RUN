package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;

import java.util.Stack;

/**
 * Created by Tadeas on 14.11.2015.
 */
public class JPopInstruction extends JInstruction {

    public JPopInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        executionFrameStack.peek().getOperandStack().pop();

        if (JVM.FULL_LOG) {
            JExecutionFrame frame = executionFrameStack.peek();
            System.out.println("Call pop instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("------------------");
        }
    }
}
