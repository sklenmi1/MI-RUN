package cz.cvut.run.instructions;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.cp.CPFieldRef;
import cz.cvut.run.structure.cp.CPNameAndType;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.heap.HeapObject;
import cz.cvut.run.vm.types.JReference;
import cz.cvut.run.vm.types.JType;

import java.nio.ByteBuffer;
import java.util.Stack;

/**
 * Created by Tadeas on 27.11.2015.
 */
public class JPutFieldInstruction extends JInstruction {

    private byte[] index;

    public JPutFieldInstruction(int opCode, byte[] index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JClass actualClass = frame.getMethod().getJClass();

        JType field = frame.getOperandStack().pop();
        JReference reference = (JReference) frame.getOperandStack().pop();

        JClass objectClass = Heap.getInstance().getObjectClass(reference);

        CPFieldRef fieldRef = (CPFieldRef) actualClass.getFromConstantPool(ByteBuffer.wrap(index).getShort());
        CPNameAndType fieldNameAndType = (CPNameAndType) actualClass.getFromConstantPool(fieldRef.getNameAndTypeIndex());

        String fieldName = actualClass.getFromConstantPool(fieldNameAndType.getNameIndex()).toString();
        String fieldDescriptor = actualClass.getFromConstantPool(fieldNameAndType.getDescriptorIndex()).toString();
        int fieldIndex = objectClass.getFieldIndex(fieldName, fieldDescriptor);

        Heap.getInstance().putField(reference, fieldIndex, field);

        if (JVM.FULL_LOG) {
            System.out.println("Call putfield instruction for field " + fieldName + " from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("------------------");
        }
    }
}
