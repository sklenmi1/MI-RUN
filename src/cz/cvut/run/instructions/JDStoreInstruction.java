package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JDouble;
import cz.cvut.run.vm.types.JInteger;

import java.util.Stack;

/**
 * Created by Tadeas on 14.11.2015.
 */
public class JDStoreInstruction extends JInstruction {

    private byte index;

    public JDStoreInstruction(int opCode, byte index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JDouble operand = (JDouble) frame.getOperandStack().pop();
        frame.addToLocalVariables(index & 0xFF, operand);

        if (JVM.FULL_LOG) {
            System.out.println("Call dstore instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On position " + (index & 0xFF) + " in local variables is: " + frame.getFromLocalVariables(index & 0xFF));
            System.out.println("------------------");
        }
    }
}
