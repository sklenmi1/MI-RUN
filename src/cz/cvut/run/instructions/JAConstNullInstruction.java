package cz.cvut.run.instructions;

import cz.cvut.run.util.HeapUtil;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JLong;
import cz.cvut.run.vm.types.JReference;

import java.util.Stack;

/**
 * Created by michal.sklenar on 12/12/15.
 */
public class JAConstNullInstruction extends JInstruction {

    public JAConstNullInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        frame.getOperandStack().push(JReference.NULL);

        if (JVM.FULL_LOG) {
            System.out.println("Call aconst_null instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek() + " with value : " + ((JReference)frame.getOperandStack().peek()).getReference());
            System.out.println("------------------");
        }
    }
}
