package cz.cvut.run.instructions;

import cz.cvut.run.util.HeapUtil;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JReference;
import cz.cvut.run.vm.types.JType;

import java.util.Stack;

/**
 * Created by Tadeas on 14.11.2015.
 */
public class JIAStoreInstruction extends JInstruction {

    public JIAStoreInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();

        JInteger value = (JInteger) frame.getOperandStack().pop();
        JInteger index = (JInteger) frame.getOperandStack().pop();
        JReference arrayRef = (JReference) frame.getOperandStack().pop();

        Heap.getInstance().putArrayField(arrayRef, index.getValue(), value);

        if (JVM.FULL_LOG) {
            System.out.println("Call iastore instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("------------------");
        }
    }
}
