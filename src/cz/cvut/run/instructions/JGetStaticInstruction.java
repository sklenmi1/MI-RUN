package cz.cvut.run.instructions;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.cp.CPClass;
import cz.cvut.run.structure.cp.CPFieldRef;
import cz.cvut.run.structure.cp.CPNameAndType;
import cz.cvut.run.structure.field.JField;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JType;

import java.nio.ByteBuffer;
import java.util.Stack;

/**
 * Created by Tadeas on 27.11.2015.
 */
public class JGetStaticInstruction extends JInstruction {

    private byte[] index;

    public JGetStaticInstruction(int opCode, byte[] index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JClass actualClass = frame.getMethod().getJClass();

        CPFieldRef fieldRef = (CPFieldRef) actualClass.getFromConstantPool(ByteBuffer.wrap(index).getShort());
        CPClass fieldClassIndex = (CPClass) actualClass.getFromConstantPool(fieldRef.getClassIndex());
        CPNameAndType fieldNameAndType = (CPNameAndType) actualClass.getFromConstantPool(fieldRef.getNameAndTypeIndex());

        String fieldClassName = actualClass.getFromConstantPool(fieldClassIndex.getNameIndex()).toString();
        String fieldName = actualClass.getFromConstantPool(fieldNameAndType.getNameIndex()).toString();
        String fieldDescriptor = actualClass.getFromConstantPool(fieldNameAndType.getDescriptorIndex()).toString();

        JClass fieldClass = JVM.getInstance().getClass(fieldClassName);
        JField jField = fieldClass.getField(fieldName, fieldDescriptor);

        if (jField == null) {
            throw new RuntimeException("Could non find static field! - " + fieldName + " " + fieldDescriptor);
        }

        if (jField.isStatic()) {
            frame.getOperandStack().push(jField.getValue());
        } else {
            throw new RuntimeException("Trying to load from non-static field inside GetStatic instruction!");
        }

        if (JVM.FULL_LOG) {
            System.out.println("Call getstatic instruction for field " + fieldName + " from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("------------------");
        }
    }
}
