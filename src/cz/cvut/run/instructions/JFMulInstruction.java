package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JFloat;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JFMulInstruction extends JInstruction {

    public JFMulInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JFloat op1 = (JFloat) frame.getOperandStack().pop();
        JFloat op2 = (JFloat) frame.getOperandStack().pop();

        frame.getOperandStack().push(op2.multiply(op1));

        if (JVM.FULL_LOG) {
            System.out.println("Call fmul instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
