package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JFloat;
import cz.cvut.run.vm.types.JLong;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JFConstXInstruction extends JInstruction {

    private float value;

    public JFConstXInstruction(int opCode, float value) {
        super(opCode);
        this.value = value;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JFloat operand = new JFloat(value);
        executionFrameStack.peek().getOperandStack().push(operand);

        if (JVM.FULL_LOG) {
            JExecutionFrame frame = executionFrameStack.peek();
            System.out.println("Call fconst_" + value + " instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
