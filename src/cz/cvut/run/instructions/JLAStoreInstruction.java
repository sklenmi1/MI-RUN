package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JDouble;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JLong;
import cz.cvut.run.vm.types.JReference;

import java.util.Stack;

/**
 * Created by Tadeas on 14.11.2015.
 */
public class JLAStoreInstruction extends JInstruction {

    public JLAStoreInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();

        JLong value = (JLong) frame.getOperandStack().pop();
        JInteger index = (JInteger) frame.getOperandStack().pop();
        JReference arrayRef = (JReference) frame.getOperandStack().pop();

        Heap.getInstance().putArrayField(arrayRef, index.getValue(), value);

        if (JVM.FULL_LOG) {
            System.out.println("Call lastore instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("------------------");
        }
    }
}
