package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JFloat;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JFLoadXInstruction extends JInstruction {

    private int index;

    public JFLoadXInstruction(int opCode, int index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JFloat operand = (JFloat) frame.getFromLocalVariables(index);
        frame.getOperandStack().push(operand);

        if (JVM.FULL_LOG) {
            JExecutionFrame newFrame = executionFrameStack.peek();
            System.out.println("Call fload_" + index + " instruction from method " + newFrame.getMethod().getName() + " from class " + newFrame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + newFrame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
