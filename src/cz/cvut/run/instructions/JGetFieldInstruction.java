package cz.cvut.run.instructions;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.cp.CPFieldRef;
import cz.cvut.run.structure.cp.CPNameAndType;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JReference;
import cz.cvut.run.vm.types.JType;

import java.nio.ByteBuffer;
import java.util.Stack;

/**
 * Created by Tadeas on 27.11.2015.
 */
public class JGetFieldInstruction extends JInstruction {

    private byte[] index;

    public JGetFieldInstruction(int opCode, byte[] index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JClass actualClass = frame.getMethod().getJClass();

        JReference reference = (JReference) frame.getOperandStack().pop();

        CPFieldRef fieldRef = (CPFieldRef) actualClass.getFromConstantPool(ByteBuffer.wrap(index).getShort());
        CPNameAndType fieldNameAndType = (CPNameAndType) actualClass.getFromConstantPool(fieldRef.getNameAndTypeIndex());

        String fieldName = actualClass.getFromConstantPool(fieldNameAndType.getNameIndex()).toString();
        String fieldDescriptor = actualClass.getFromConstantPool(fieldNameAndType.getDescriptorIndex()).toString();

        JClass objectClass = Heap.getInstance().getObjectClass(reference);
        int fieldIndex = objectClass.getFieldIndex(fieldName, fieldDescriptor);


        JType field = Heap.getInstance().getField(reference, fieldIndex);
        frame.getOperandStack().push(field);

        if (JVM.FULL_LOG) {
            System.out.println("Call getfield instruction for field " + fieldName + " from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("------------------");
        }
    }
}
