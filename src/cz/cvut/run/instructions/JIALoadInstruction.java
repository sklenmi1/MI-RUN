package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JDouble;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JReference;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JIALoadInstruction extends JInstruction {

    public JIALoadInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();

        JInteger index = (JInteger) frame.getOperandStack().pop();
        JReference arrayRef = (JReference) frame.getOperandStack().pop();

        JInteger value = (JInteger) Heap.getInstance().getArrayField(arrayRef, index.getValue());

        frame.getOperandStack().push(value);

        if (JVM.FULL_LOG) {
            System.out.println("Call iaload instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
