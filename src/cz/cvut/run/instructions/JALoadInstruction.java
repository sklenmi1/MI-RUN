package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JLong;
import cz.cvut.run.vm.types.JReference;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JALoadInstruction extends JInstruction {

    private byte index;

    public JALoadInstruction(int opCode, byte index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JReference operand = (JReference) frame.getFromLocalVariables(index & 0xFF);
        frame.getOperandStack().push(operand);

        if (JVM.FULL_LOG) {
            System.out.println("Call aload instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek() + " with value : " + ((JReference) frame.getOperandStack().peek()).getReference());
            System.out.println("------------------");
        }
    }
}
