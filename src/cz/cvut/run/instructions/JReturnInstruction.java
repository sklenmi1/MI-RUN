package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;

import java.util.Stack;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JReturnInstruction extends JInstruction {
    public JReturnInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();

        while (!frame.getOperandStack().isEmpty()) {
            System.out.println(frame.getOperandStack().peek().toString());
            frame.getOperandStack().pop();
        }
        /**
         * logging
         */
        if (JVM.FULL_LOG) {
            System.out.println("Call JReturn from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            for (int a = 0; a < frame.getLocalVariables().length; a++) {
                System.out.println("Variable on index " + a + ": " + frame.getFromLocalVariables(a));
            }

            System.out.println("------------------");
        }
        executionFrameStack.pop();
    }
}
