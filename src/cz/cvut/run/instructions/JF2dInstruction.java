package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JDouble;
import cz.cvut.run.vm.types.JFloat;

import java.util.Stack;

/**
 * Created by michal.sklenar on 18/11/15.
 */
public class JF2dInstruction extends JInstruction {

    public JF2dInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JFloat value = (JFloat) frame.getOperandStack().pop();
        frame.getOperandStack().push(new JDouble((double) value.getValue()));

        if (JVM.FULL_LOG) {
            System.out.println("Call f2d instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
