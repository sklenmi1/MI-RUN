package cz.cvut.run.instructions;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.cp.CPClass;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JReference;

import java.nio.ByteBuffer;
import java.util.Stack;

/**
 * Created by michal.sklenar on 13/12/15.
 */
public class JInstanceOfInstruction extends JInstruction {

    private byte [] index;

    public JInstanceOfInstruction(int opCode, byte [] index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame actualFrame = executionFrameStack.peek();
        JClass actualClass = actualFrame.getMethod().getJClass();
        String className = actualClass.getFromConstantPool(((CPClass) actualClass.getFromConstantPool(ByteBuffer.wrap(index).getShort())).getNameIndex()).toString();

        if (className.equals("java/lang/Object")) {
            actualFrame.getOperandStack().push(new JInteger(1));
            return;
        }

        JClass refClass = JVM.getInstance().getClass(className);


        JReference objectRef = (JReference) actualFrame.getOperandStack().pop();
        JClass objectClass = Heap.getInstance().getObjectClass(objectRef);

        if (objectClass.isInstanceOf(refClass)) {
            actualFrame.getOperandStack().push(new JInteger(1));
        } else {
            actualFrame.getOperandStack().push(new JInteger(0));
        }

        if (JVM.FULL_LOG) {
            JExecutionFrame frame = executionFrameStack.peek();
            System.out.println("Call instanceof instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
