package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JType;

import java.util.Stack;

/**
 * Created by michal.sklenar on 18/11/15.
 */
public class JDupX1Instruction extends JInstruction {
    public JDupX1Instruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JType valueOne = frame.getOperandStack().pop();
        JType valueTwo = frame.getOperandStack().pop();

        frame.getOperandStack().push(valueOne.copy());
        frame.getOperandStack().push(valueTwo);
        frame.getOperandStack().push(valueOne);

        if (JVM.FULL_LOG) {
            System.out.println("Call dup_x1 instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
