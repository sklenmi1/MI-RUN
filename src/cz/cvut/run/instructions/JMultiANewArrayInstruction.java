package cz.cvut.run.instructions;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.cp.CPClass;
import cz.cvut.run.util.Util;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JInteger;
import cz.cvut.run.vm.types.JReference;

import java.nio.ByteBuffer;
import java.util.Stack;

/**
 * Created by michal.sklenar on 12/12/15.
 */
public class JMultiANewArrayInstruction extends JInstruction {

    private byte[] index;

    public JMultiANewArrayInstruction(int opCode, byte[] index) {
        super(opCode);
        this.index = index;
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        int[] counts = new int[index[2]];

        JExecutionFrame frame = executionFrameStack.peek();
        for (int i = index[2] - 1; i >= 0; i--) {
            counts[i] = ((JInteger) frame.getOperandStack().pop()).getValue();
        }

        JClass actualClass = frame.getMethod().getJClass();
        CPClass classRef = (CPClass) actualClass.getFromConstantPool(ByteBuffer.wrap(index).getShort());
        String className = actualClass.getFromConstantPool(classRef.getNameIndex()).toString();

        String refClassName = Util.parseClassName(className);
        int atype = Util.getAtype(refClassName);

        JReference reference;
        if (atype > 0) {
            reference = Heap.getInstance().allocateArray(atype, counts);
        } else {
            JClass refClass = JVM.getInstance().getClass(refClassName.substring(1));
            reference = Heap.getInstance().allocateArray(refClass, counts);
        }

        frame.getOperandStack().push(reference);

        if (JVM.FULL_LOG) {
            System.out.println("Call multinewarray instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
