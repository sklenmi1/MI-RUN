package cz.cvut.run.instructions;

import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JFloat;
import cz.cvut.run.vm.types.JInteger;

import java.util.Stack;

/**
 * Created by michal.sklenar on 18/11/15.
 */
public class JFCmpgInstruction extends JInstruction {
    public JFCmpgInstruction(int opCode) {
        super(opCode);
    }

    @Override
    public void execute(Stack<JExecutionFrame> executionFrameStack) {
        JExecutionFrame frame = executionFrameStack.peek();
        JFloat op1 = (JFloat) frame.getOperandStack().pop();
        JFloat op2 = (JFloat) frame.getOperandStack().pop();

        if (op2.getValue() > op1.getValue()) {
            frame.getOperandStack().push(new JInteger(1));
        } else if (op2.getValue() == op1.getValue()) {
            frame.getOperandStack().push(new JInteger(0));
        } else {
            frame.getOperandStack().push(new JInteger(-1));
        }

        if (JVM.FULL_LOG) {
            System.out.println("Call fcmpg instruction from method " + frame.getMethod().getName() + " from class " + frame.getMethod().getJClass().getClassName());
            System.out.println("On peek of operand stack is: " + frame.getOperandStack().peek());
            System.out.println("------------------");
        }
    }
}
