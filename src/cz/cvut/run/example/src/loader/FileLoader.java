package cz.cvut.run.example.src.loader;

import cz.cvut.run.example.src.sat.Clause;
import cz.cvut.run.example.src.sat.Formula;
import cz.cvut.run.example.src.sat.Literal;
import cz.cvut.run.example.src.util.Util;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by michal.sklenar on 30/12/15.
 */
public class FileLoader {

    public FileLoader() {
    }

    public Formula load(String path) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(path), null);

        String line;
        Formula formula = new Formula();
        Clause[] clauses = null;
        int index = 0;

        for (int lineIndex = 0; lineIndex < lines.size(); lineIndex++) {
            line = lines.get(lineIndex);
            String[] parseLine = Util.split(line, " ");

            if (parseLine[0].equals("c")) {
                continue;
            }

            if (parseLine[0].equals("p")) {
                formula.setLiteralsCount(Util.strToInt(parseLine[2]));
                formula.setClausesCount(Util.strToInt(parseLine[3]));
                clauses = new Clause[formula.getClausesCount()];
                continue;
            }

            if (parseLine[0].equals("w")) {
                formula.setWeightsFromFile(parseLine);
                continue;
            }

            Clause clause = new Clause();
            Literal[] literals = new Literal[parseLine.length - 1];
            for (int i = 0; i < formula.getLiteralsCount(); i++) {
                if (Util.strToInt(parseLine[i]) == 0) {
                    break;
                }

                int parsedLiteral = Util.strToInt(parseLine[i]);
                Literal literal = new Literal(parsedLiteral);
                literals[i] = literal;
            }

            clause.setLiterals(literals);
            clause.setLiteralsWeights(formula.getWeights());
            clauses[index] = clause;
            index++;

        }
        formula.setClauses(clauses);
        return formula;
    }
}
