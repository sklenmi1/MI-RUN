package cz.cvut.run.example.src.sat;

/**
 * Created by michal.sklenar on 30/12/15.
 */
public class Literal {
    private int id;
    private boolean isNegative;
    private int weight;

    public Literal(int literal) {
        if (literal < 0) {
            this.isNegative = true;
        }

        if (literal > 0) {
            this.isNegative = false;
        }

        this.id = Math.abs(literal);
        this.weight = 0;
    }

    public int getId() {
        return id;
    }

    public boolean isNegative() {
        return isNegative;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
