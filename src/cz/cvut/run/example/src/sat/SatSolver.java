package cz.cvut.run.example.src.sat;

import cz.cvut.run.example.src.algorithm.BruteForceAlgorithm;
import cz.cvut.run.example.src.loader.FileLoader;
import cz.cvut.run.example.src.util.Util;

import java.io.IOException;

/**
 * Created by michal.sklenar on 30/12/15.
 */
public class SatSolver {
    private FileLoader loader;

    private Formula satProblem;

    public SatSolver() {
        this.loader = new FileLoader();
    }

    public Formula getSatProblem() {
        return satProblem;
    }

    public void setSatProblem(Formula satProblem) {
        this.satProblem = satProblem;
    }

    public void solve(String path) throws IOException {
        setSatProblem(loader.load(path));

        System.out.println("Loading done");

        BruteForceAlgorithm bfa = new BruteForceAlgorithm(getSatProblem());
        Formula bfSolution = bfa.solve();

        System.out.println("Formula satisfied : " + bfSolution.isSatisfiable());
        System.out.println("BruteForce solution weight = " + bfSolution.getSumWeight());
        System.out.println("BruteForce solution : " + Util.convertArrayToBinaryString(bfSolution.getLiteralValues(), bfSolution.getLiteralsCount()));
    }
}
