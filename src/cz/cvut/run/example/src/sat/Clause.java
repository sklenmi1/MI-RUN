package cz.cvut.run.example.src.sat;

import java.util.Arrays;

/**
 * Created by michal.sklenar on 30/12/15.
 */
public class Clause {

    private Literal[] literals;

    public Clause() {
    }

    public Literal[] getLiterals() {
        return literals;
    }

    public void setLiterals(Literal[] literals) {
        this.literals = literals;
    }

    public void setLiteralsWeights(int [] weights) {
        for (int i = 0; i < literals.length; i++) {
            literals[i].setWeight(weights[literals[i].getId()]);
        }
    }

    public boolean isSatisfiable(boolean [] literalValues) {
        boolean sat = false;
        for (int i = 1; i <= literals.length; i++) {
            if ((!literals[i - 1].isNegative() && literalValues[literals[i - 1].getId()]) || (literals[i - 1].isNegative() && !literalValues[literals[i - 1].getId()])) {
                sat = true;
            }
        }

        return sat;
    }
}
