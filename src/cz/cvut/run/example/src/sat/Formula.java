package cz.cvut.run.example.src.sat;

import cz.cvut.run.example.src.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal.sklenar on 30/12/15.
 */
public class Formula {
    private Clause [] clauses;
    private int [] weights;
    private int clausulesCount;
    private int literalsCount;
    private boolean [] literalValues;
    private int sumWeight;

    public Formula() {
        this.clausulesCount = 0;
        this.literalsCount = 0;
        this.sumWeight = -1;
    }

    public Formula(Formula oldFormula) {
        this.clauses = oldFormula.getClauses();
        this.clausulesCount = oldFormula.getClausesCount();
        this.literalsCount = oldFormula.getLiteralsCount();
        this.sumWeight = oldFormula.getSumWeight();
        this.weights = oldFormula.getWeights();

        this.literalValues = new boolean[oldFormula.getLiteralValues().length];
        System.arraycopy(oldFormula.getLiteralValues(), 0, this.literalValues, 0, oldFormula.getLiteralValues().length);
    }

    public Clause [] getClauses() {
        return clauses;
    }

    public void setClauses(Clause[] clauses) {
        this.clauses = clauses;
    }

    public int getClausesCount() {
        return clausulesCount;
    }

    public void setClausesCount(int clausulesCount) {
        this.clausulesCount = clausulesCount;
    }

    public int getLiteralsCount() {
        return literalsCount;
    }

    public void setLiteralsCount(int literalsCount) {
        this.literalsCount = literalsCount;
        this.literalValues = new boolean[literalsCount + 1];
    }

    public boolean isSatisfiable() {
        for (int i = 0; i < clausulesCount; i++) {
            if (!clauses[i].isSatisfiable(literalValues)) {
                return false;
            }
        }

        return true;
    }

    public int getSumWeight() {

            calculateSumWeight();

        return sumWeight;
    }

    public void calculateSumWeight() {
        int weight = 0;
        for (int i = 1; i <= literalsCount; i++) {
            weight += literalValues[i] ? weights[i] : 0;
        }

        sumWeight = weight;
    }

    public int [] getWeights() {
        return weights;
    }

    public boolean [] getLiteralValues() {
        return literalValues;
    }

    public void setWeightsFromFile(String[] parseLine) {
        weights = new int[literalsCount + 1];
        weights[0] = 0;
        for (int i = 1; i < parseLine.length; i++) {
            weights[i] = Util.strToInt(parseLine[i]);
        }
    }
}
