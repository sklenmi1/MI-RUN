package cz.cvut.run.example.src.util;

/**
 * Created by michal.sklenar on 30/12/15.
 */
public class Util {

    public static String convertArrayToBinaryString(boolean[] literalValues, int literalsCount) {
        String binString = "";
        for (int i = 1; i <= literalsCount; i++) {
            if (literalValues[i]) {
                binString += "1 ";
            }

            if (!literalValues[i]) {
                binString += "0 ";
            }
        }

        return binString;
    }


    public static String[] split(String str, String c) //split string function
    {
        int lenght = str.length();
        int first = 0;
        int last = 0;
        int count = 0;
        for (int i = 0; i < lenght; i++) {
            if (str.substring(i, i + 1).equals(c)) {
                count++;
            }
        }

        String[] array = new String[count + 1];
        int pos = 0;
        for (int i = 0; i < lenght; i++) {
            if (str.substring(i, i + 1).equals(c)) {
                last = i;
                array[pos++] = str.substring(first, last);
                first = last + 1;
            }
        }

        array[pos] = str.substring(first);

        return array;
    }

    public static int strToInt(String str) {
        int i = 0;
        int num = 0;
        boolean isNeg = false;

        //Check for negative sign; if it's there, set the isNeg flag
        if (str.charAt(0) == '-') {
            isNeg = true;
            i = 1;
        }

        //Process each character of the string;
        while (i < str.length()) {
            num *= 10;
            num += str.charAt(i++) - '0'; //Minus the ASCII code of '0' to get the value of the charAt(i++).
        }

        if (isNeg)
            num = -num;
        return num;
    }
}
