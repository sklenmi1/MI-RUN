package cz.cvut.run.example.src.algorithm;


import cz.cvut.run.example.src.sat.Formula;

/**
 * Created by michal.sklenar on 30/12/15.
 */
public class BruteForceAlgorithm {

    private Formula formulaInstance;
    private Formula bestSolution;

    public BruteForceAlgorithm(Formula formulaInstance) {
        this.formulaInstance = formulaInstance;
    }

    public Formula solve() {
        bestSolution = new Formula(formulaInstance);
        recursiveSolve(1, false, formulaInstance);
        recursiveSolve(1, true, formulaInstance);

        return bestSolution;
    }

    private void recursiveSolve(int index, boolean value, Formula instance) {
        instance.getLiteralValues()[index] = value;
        if (index < instance.getLiteralsCount()) {
            recursiveSolve(index + 1, false, instance);
            recursiveSolve(index + 1, true, instance);
        } else {
            if (instance.isSatisfiable() && instance.getSumWeight() > getBestSolution().getSumWeight()) {
                bestSolution = new Formula(instance);
            }
        }
    }

    public Formula getBestSolution() {
        return bestSolution;
    }
}
