package cz.cvut.run.example.src;

import cz.cvut.run.example.src.sat.SatSolver;

import java.io.IOException;

/**
 * Created by michal.sklenar on 30/12/15.
 */
public class Main {

    public static void main(String[] args) throws IOException {
        if(args.length != 1){
            throw new RuntimeException("Set argument for path to test_instance.cnf!");
        }



        System.out.println("Trying to load this file to SAT - " + args[0]);
        String path = args[0];

        try {
            SatSolver ss = new SatSolver();
            //ss.solve("D:/Dropbox (SKOUMAL, s.r.o.)/Skola/RUN/MI-RUN/data/cz/cvut/run/example/data/test_instance.cnf");
            ss.solve(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
