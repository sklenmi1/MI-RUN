package cz.cvut.run.vm;

import cz.cvut.run.instructions.JInstruction;
import cz.cvut.run.loader.JInstructionReader;
import cz.cvut.run.structure.method.JMethod;
import cz.cvut.run.vm.types.JType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * Created by Tadeas on 12.11.2015.
 */
public class JExecutionFrame {

    protected JMethod method;

    protected Stack<JType> operandStack;
    protected JType[] localVariables;

    protected int pc;
    protected byte[] bytecode;
    protected int bytecodeLength;

    protected boolean isNative;

    public JExecutionFrame(JMethod method) {
        this.method = method;
        this.operandStack = new Stack<>();
        this.pc = 0;
        if (!method.isNative()) {
            this.localVariables = new JType[method.getCodeAttribute().getMaxLocals()];
            this.bytecode = method.getCodeAttribute().getInstructions();
            this.bytecodeLength = method.getCodeAttribute().getCodeLength();
        }
        this.isNative = false;
    }

    public void addOffsetInProgramCounter(int offset) {
        this.pc += offset;
    }

    public boolean hasNextInstruction() {
        return pc < bytecodeLength;
    }

    public JInstruction nextInstruction() {
        return (pc >= bytecodeLength) ? null : JInstructionReader.readInstruction(this);
    }

    public Stack<JType> getOperandStack() {
        return operandStack;
    }

    public JType[] getLocalVariables() {
        return localVariables;
    }

    public JMethod getMethod() {
        return method;
    }

    public void addToLocalVariables(int index, JType operand) {
        localVariables[index] = operand;
    }

    public JType getFromLocalVariables(int index) {
        return localVariables[index];
    }

    public byte getNextByte() {
        return bytecode[pc++];
    }

    public byte[] getNextNBytes(int n) {
        byte[] bytes = Arrays.copyOfRange(bytecode, pc, pc + n);
        pc += n;
        return bytes;
    }

    public int getProgramCounter() {
        return pc;
    }

    public void setProgramCounter(int pc) {
        this.pc = pc;
    }

    public boolean isNative() {
        return isNative;
    }

    public int getLocalVariablesSize() {
        return localVariables == null ? 0 : localVariables.length;
    }
}
