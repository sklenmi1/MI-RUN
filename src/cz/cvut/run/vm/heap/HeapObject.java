package cz.cvut.run.vm.heap;

import cz.cvut.run.vm.types.*;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by Tadeas on 18.11.2015.
 */
public class HeapObject {
    public static final int HEADER_SIZE = 12;
    public static final int OBJECT_REF_TAG = 1;
    public int tag;
    public int classIndexInMethodArea;
    public int fieldsSize;
    public ArrayList<JType> fields;

    public HeapObject(int classIndexInMethodArea) {
        this.tag = 1;
        this.classIndexInMethodArea = classIndexInMethodArea;
        this.fieldsSize = 0;
        this.fields = new ArrayList<>();
    }

    public HeapObject(int tag, int classIndexInMethodArea) {
        this.tag = tag;
        this.classIndexInMethodArea = classIndexInMethodArea;
        this.fieldsSize = 0;
        fields = new ArrayList<>();
    }

    public static HeapObject getFromBytes(byte[] bytes) {
        return getFromBytes(bytes, 0);
    }

    public static HeapObject getFromBytes(byte[] bytes, int offset) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        buffer.position(offset);
        int tag = buffer.getInt();
        int classIndexInMethodArea = buffer.getInt();

        HeapObject object = new HeapObject(tag, classIndexInMethodArea);

        JType field = null;
        byte fieldType;
        byte[] fieldBytes = new byte[8];
        int fieldSize = buffer.getInt();
        while (fieldSize > 0) {
            fieldType = buffer.get();
            switch (fieldType) {
                case JType.JDouble:
                    field = new JDouble(buffer.getDouble());
                    break;
                case JType.JFloat:
                    field = new JFloat(buffer.getFloat());
                    break;
                case JType.JInteger:
                    field = new JInteger(buffer.getInt());
                    break;
                case JType.JLong:
                    field = new JLong(buffer.getLong());
                    break;
                case JType.JReference:
                    field = new JReference(buffer.getInt());
                    break;
            }
            object.addField(field);
            fieldSize -= (1 + field.getBytesLength());
        }

        return object;
    }

    public byte[] toBytes() {
        for (JType field : fields) {
            fieldsSize += field.getBytesLength();
        }
        int actualSize = HEADER_SIZE;
        int copyLength;
        byte[] bytes = new byte[HEADER_SIZE + fieldsSize];

        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        buffer.putInt(tag);
        buffer.putInt(classIndexInMethodArea);
        buffer.putInt(fieldsSize);
        //System.arraycopy(Ints.toByteArray(tag), 0, bytes, 0, 4);
        //System.arraycopy(Ints.toByteArray(classIndexInMethodArea), 0, bytes, 4, 4);
        //System.arraycopy(Ints.toByteArray(fieldsSize), 0, bytes, 8, 4);
        for (JType field : fields) {
            System.arraycopy(field.toBytes(), 0, bytes, actualSize, copyLength = field.getBytesLength());
            actualSize += copyLength;
        }
        return bytes;
    }

    /**
     * it's bytes size of fields, bytes for tag and size attribute are not included
     *
     * @return
     */
    public int getFieldsSize() {
        return fieldsSize;
    }

    public void addField(JType field) {
        if (field == null) {
            throw new RuntimeException("trying to add null field!");
        }

        fields.add(field);
        fieldsSize++;
    }

    public JType getField(int index) {
        return fields.get(index);
    }
}
