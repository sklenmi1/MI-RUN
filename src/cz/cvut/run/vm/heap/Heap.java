package cz.cvut.run.vm.heap;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.util.HeapUtil;
import cz.cvut.run.util.Util;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.*;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Created by Tadeas on 18.11.2015.
 */
public class Heap {
    private static final int INIT_SIZE = 200000;

    private static Heap instance;

    private byte[] heapArrayA;
    private byte[] heapArrayB;

    private boolean isActiveArrayA;


    private int nextFreeSpacePointerArrayA;
    private int nextFreeSpacePointerArrayB;

    private int nextFreeSpacePointer;
    private int actualHeapSize;

    private ByteBuffer byteBuffer;

    private Heap() {
        init();
    }

    public static Heap getInstance() {
        if (instance == null) {
            instance = new Heap();
        }
        return instance;
    }

    public int getNextFreeSpacePointer(){
        return nextFreeSpacePointer;
    }

    public JReference allocateObject(JClass objectClass) {
        int spaceNeeded = Util.spaceNeededInHeap(objectClass);

        if (nextFreeSpacePointer + spaceNeeded >= 0.75f * actualHeapSize) {
            GarbageCollector.start();

            if (nextFreeSpacePointer + spaceNeeded >= actualHeapSize) {
                throw new RuntimeException("Heap overflow! OutOfMemory!");
            }
        }

        byteBuffer.position(nextFreeSpacePointer);

        byteBuffer.putInt(HeapObject.OBJECT_REF_TAG);
        byteBuffer.putInt(objectClass.getIndexInMethodArea());
        byteBuffer.putInt(spaceNeeded - HeapObject.HEADER_SIZE);

        JReference objectRef = new JReference(nextFreeSpacePointer);
        nextFreeSpacePointer += spaceNeeded;

        return objectRef;
    }

    public int moveObjectToSecondHeapArray(JReference reference) {
        byteBuffer.position(reference.getReference() + 8);
        int objectSize = byteBuffer.getInt();
        int pointer;

        if (!HeapUtil.isObjectReference(reference)) {
            objectSize *= 4;
            if (HeapUtil.isPrimitiveArray(reference)) {
                int atype = HeapUtil.getArrayAType(reference);
                if ((atype == 7) || (atype == 11)) {
                    objectSize *= 2;
                }
            }
        }

        objectSize += HeapObject.HEADER_SIZE;
        if (isActiveArrayA) {
            System.arraycopy(heapArrayA, reference.getReference(), heapArrayB, pointer = nextFreeSpacePointerArrayB, objectSize);
            nextFreeSpacePointerArrayB += objectSize;
        } else {
            System.arraycopy(heapArrayB, reference.getReference(), heapArrayA, pointer = nextFreeSpacePointerArrayA, objectSize);
            nextFreeSpacePointerArrayA += objectSize;
        }
        return pointer;
    }

    public JReference allocateArray(JClass type, int size) {
        int spaceNeeded = Util.spaceNeedInHeapForArray(size);

        if (nextFreeSpacePointer + spaceNeeded >= 0.75f * actualHeapSize) {
            GarbageCollector.start();

            if (nextFreeSpacePointer + spaceNeeded >= actualHeapSize) {
                throw new RuntimeException("Heap overflow! OutOfMemory!");
            }
        }

        byteBuffer.position(nextFreeSpacePointer);

        byteBuffer.putInt(HeapArray.OBJECT_ARRAY_REF_TAG);
        byteBuffer.putInt(type.getIndexInMethodArea());
        byteBuffer.putInt(size);

        for (int i = 0; i < size; i++) {
            byteBuffer.putInt(JReference.NULL_VAL);
        }

        JReference arrayRef = new JReference(nextFreeSpacePointer);
        nextFreeSpacePointer += spaceNeeded;

        return arrayRef;
    }

    public JReference allocateArray(JClass type, int[] size) {
        return allocateArray(type, size, 0);
    }

    private JReference allocateArray(JClass type, int[] size, int offset) {
        int spaceNeeded = Util.spaceNeedInHeapForArray(size[offset]);

        if (nextFreeSpacePointer + spaceNeeded >= 0.75f * actualHeapSize) {
            GarbageCollector.start();

            if (nextFreeSpacePointer + spaceNeeded >= actualHeapSize) {
                throw new RuntimeException("Heap overflow! OutOfMemory!");
            }
        }

        int actualPointer;

        JReference arrayRef = new JReference(actualPointer = nextFreeSpacePointer);
        nextFreeSpacePointer += spaceNeeded;

        byteBuffer.position(actualPointer);
        byteBuffer.putInt(HeapArray.OBJECT_MULTI_ARRAY_REF_TAG);
        byteBuffer.putInt(type.getIndexInMethodArea());
        byteBuffer.putInt(size[offset]);

        actualPointer += HeapArray.HEADER_SIZE;

        boolean subArrayIsMulti;
        if (offset + 2 < size.length) {
            subArrayIsMulti = true;
        } else {
            subArrayIsMulti = false;
        }

        JReference subArrayRef;
        for (int i = 0; i < size[offset]; i++) {
            if (subArrayIsMulti) {
                subArrayRef = allocateArray(type, size, offset++);
                byteBuffer.position(actualPointer);
                byteBuffer.put(subArrayRef.toBytesWithoutTag());
                actualPointer += 4;
            } else {
                subArrayRef = allocateArray(type, size[offset + 1]);
                byteBuffer.position(actualPointer);
                byteBuffer.put(subArrayRef.toBytesWithoutTag());
                actualPointer += 4;
            }
        }

        return arrayRef;
    }


    public JReference allocateArray(int atype, int size) {
        int spaceNeeded = Util.spaceNeedInHeapForArray(atype, size);

        if (nextFreeSpacePointer + spaceNeeded >= 0.75f * actualHeapSize) {
            GarbageCollector.start();

            if (nextFreeSpacePointer + spaceNeeded >= actualHeapSize) {
                throw new RuntimeException("Heap overflow! OutOfMemory!");
            }
        }

        byteBuffer.position(nextFreeSpacePointer);

        byteBuffer.putInt(HeapArray.PRIMITIVE_ARRAY_REF_TAG);
        byteBuffer.putInt(atype);
        byteBuffer.putInt(size);

        JReference arrayRef = new JReference(nextFreeSpacePointer);
        nextFreeSpacePointer += spaceNeeded;

        return arrayRef;
    }

    public JReference allocateArray(int atype, int[] size) {
        return allocateArray(atype, size, 0);
    }

    private JReference allocateArray(int atype, int[] size, int offset) {
        int spaceNeeded = Util.spaceNeedInHeapForArray(atype, size[offset]);

        if (nextFreeSpacePointer + spaceNeeded >= 0.75f * actualHeapSize) {
            GarbageCollector.start();

            if (nextFreeSpacePointer + spaceNeeded >= actualHeapSize) {
                throw new RuntimeException("Heap overflow! OutOfMemory!");
            }
        }
        int actualPointer;

        JReference arrayRef = new JReference(actualPointer = nextFreeSpacePointer);
        nextFreeSpacePointer += spaceNeeded;

        byteBuffer.position(actualPointer);
        byteBuffer.putInt(HeapArray.PRIMITIVE_MULTI_ARRAY_REF_TAG);
        byteBuffer.putInt(atype);
        byteBuffer.putInt(size[offset]);

        actualPointer += HeapArray.HEADER_SIZE;

        boolean subArrayIsMulti;
        if (offset + 2 < size.length) {
            subArrayIsMulti = true;
        } else {
            subArrayIsMulti = false;
        }

        JReference subArrayRef;
        for (int i = 0; i < size[offset]; i++) {
            if (subArrayIsMulti) {
                subArrayRef = allocateArray(atype, size, offset++);
                byteBuffer.position(actualPointer);
                byteBuffer.put(subArrayRef.toBytesWithoutTag());
                actualPointer += 4;
            } else {
                subArrayRef = allocateArray(atype, size[offset + 1]);
                byteBuffer.position(actualPointer);
                byteBuffer.put(subArrayRef.toBytesWithoutTag());
                actualPointer += 4;
            }
        }

        return arrayRef;
    }

    public void putArrayField(JReference reference, int index, JType field) {
        int typeSize = 4;
        if (HeapUtil.isPrimitiveArray(reference)) {
            typeSize = Util.getAtypeSize(HeapUtil.getArrayAType(reference));
        }
        byteBuffer.position(reference.getReference() + HeapArray.HEADER_SIZE + index * typeSize);
        byteBuffer.put(field.toBytesWithoutTag());
    }

    public JType getArrayField(JReference reference, int index) {
        int typeSize = 4;
        boolean isPrimitive = HeapUtil.isPrimitiveArray(reference);
        if (isPrimitive) {
            typeSize = Util.getAtypeSize(HeapUtil.getArrayAType(reference));
        }

        if (!isPrimitive) {
            byteBuffer.position(reference.getReference() + HeapArray.HEADER_SIZE + index * typeSize);

            return new JReference(byteBuffer.getInt());
        }

        int atype = HeapUtil.getArrayAType(reference);

        byteBuffer.position(reference.getReference() + HeapArray.HEADER_SIZE + index * typeSize);
        switch (atype) {
            case 4:
            case 5:
            case 8:
            case 9:
            case 10:
                return new JInteger(byteBuffer.getInt());
            case 6:
                return new JFloat(byteBuffer.getFloat());
            case 7:
                return new JDouble(byteBuffer.getDouble());
            case 11:
                return new JLong(byteBuffer.getLong());
        }

        return null;
    }

    public HeapObject getObject(JReference reference) {
        return HeapObject.getFromBytes(isActiveArrayA ? heapArrayA : heapArrayB, reference.getReference());
    }

    public JClass getObjectClass(JReference reference) {
        byteBuffer.position(reference.getReference() + 4);
        return JVM.getInstance().getClass(byteBuffer.getInt());
    }

    public void putField(JReference reference, int fieldIndex, JType field) {
        byteBuffer.position(reference.getReference() + HeapObject.HEADER_SIZE + fieldIndex * 9);
        byteBuffer.put(field.toBytes());
    }

    public JType getField(JReference reference, int fieldIndex) {
        byteBuffer.position(reference.getReference() + HeapObject.HEADER_SIZE + fieldIndex * 9);
        byte fieldType = byteBuffer.get();
        switch (fieldType) {
            case JType.JDouble:
                return new JDouble(byteBuffer.getDouble());
            case JType.JFloat:
                return new JFloat(byteBuffer.getFloat());
            case JType.JInteger:
                return new JInteger(byteBuffer.getInt());
            case JType.JLong:
                return new JLong(byteBuffer.getLong());
            case JType.JReference:
                return new JReference(byteBuffer.getInt());
        }

        JClass jClass = getObjectClass(reference);
        JType defValue = jClass.getDefaultField(fieldIndex);
        if (defValue == null) {
            throw new RuntimeException("Null for default field value!");
        }

        return defValue;
    }

    private void init() {
        heapArrayA = new byte[actualHeapSize = INIT_SIZE];
        heapArrayB = new byte[INIT_SIZE];
        nextFreeSpacePointer = nextFreeSpacePointerArrayA = nextFreeSpacePointerArrayB = 0;
        byteBuffer = ByteBuffer.wrap(heapArrayA);
        isActiveArrayA = true;
    }


    public ByteBuffer getByteBuffer() {
        return byteBuffer;
    }

    public void swapActiveArray() {
        isActiveArrayA = !isActiveArrayA;
        nextFreeSpacePointer = isActiveArrayA ? nextFreeSpacePointerArrayA : nextFreeSpacePointerArrayB;
        nextFreeSpacePointerArrayA = nextFreeSpacePointerArrayB = 0;
        byteBuffer = isActiveArrayA ? ByteBuffer.wrap(heapArrayA) : ByteBuffer.wrap(heapArrayB);

        Arrays.fill(isActiveArrayA ? heapArrayB : heapArrayA, (byte) 0);

    }
}
