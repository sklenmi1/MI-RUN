package cz.cvut.run.vm.heap;

/**
 * Created by Tadeas on 12.12.2015.
 */
public class HeapArray {
    public static final int HEADER_SIZE = 12;
    public static final int PRIMITIVE_ARRAY_REF_TAG = 2;
    public static final int OBJECT_ARRAY_REF_TAG = 3;
    public static final int PRIMITIVE_MULTI_ARRAY_REF_TAG = 4;
    public static final int OBJECT_MULTI_ARRAY_REF_TAG = 5;

}
