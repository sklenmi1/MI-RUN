package cz.cvut.run.vm.heap;

import java.util.*;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.field.JField;
import cz.cvut.run.util.HeapUtil;
import cz.cvut.run.vm.JExecutionFrame;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JReference;
import cz.cvut.run.vm.types.JType;

/**
 * Created by Tade� on 28.12.2015.
 */
public class GarbageCollector {
    private static int actualGCRunNumber = 1;

    private static HashMap<Integer, Integer> movedObjects = new HashMap<>();
    private static List<JReference> neededReferences = new ArrayList<>();
    private static Heap heap = Heap.getInstance();

    public static void start() {
        System.out.println("----------------");
        System.out.println("STARTING GC - next empty ref = " + heap.getNextFreeSpacePointer());
        Stack<JExecutionFrame> executionFrameStack = JVM.getInstance().getExecutionStack();
        Iterator<JExecutionFrame> executionFrameStackIterator = executionFrameStack.iterator();

        JExecutionFrame frame;

        while (executionFrameStackIterator.hasNext()) {
            frame = executionFrameStackIterator.next();
            processExecutionFrame(frame);
        }

        Iterator<JClass> loadedClassesIterator = JVM.getInstance().getLoadedClass().values().iterator();
        JClass jClass;
        List<JField> fields;
        JType fieldValue;

        while (loadedClassesIterator.hasNext()) {
            jClass = loadedClassesIterator.next();
            fields = jClass.getFieldsList();

            for (JField field : fields) {
                if (!field.isStatic()) {
                    continue;
                }

                fieldValue = field.getValue();
                if (fieldValue instanceof JReference) {
                    processReference((JReference) fieldValue);
                }

            }
        }

        for (JReference reference : neededReferences){
            processReference(reference);
        }

        heap.swapActiveArray();

        movedObjects.clear();

        actualGCRunNumber++;
        System.out.println("FINISHED");
        System.out.println("----------------");
    }

    private static void processExecutionFrame(JExecutionFrame frame) {
        Stack<JType> operandStack = frame.getOperandStack();
        Iterator<JType> operandStackIterator = operandStack.iterator();

        JType operand;

        while (operandStackIterator.hasNext()) {
            operand = operandStackIterator.next();

            if (operand instanceof JReference) {
                processReference((JReference) operand);
            }
        }

        int localVariablesSize = frame.getLocalVariablesSize();
        for (int i = 0; i < localVariablesSize; i++) {
            operand = frame.getFromLocalVariables(i);

            if (operand instanceof JReference) {
                processReference((JReference) operand);
            }
        }
    }

    private static void processReference(JReference reference) {
        if (reference.isNull() || (reference.getMark() == actualGCRunNumber)) {
            return;
        }

        if (movedObjects.containsKey(reference.getReference())) {
            reference.setReference(movedObjects.get(reference.getReference()));
        } else {
            boolean isObjectReference = HeapUtil.isObjectReference(reference);
            JType field;

            if (isObjectReference) {
                int fieldsCount = heap.getObjectClass(reference).getFieldsCount();
                for (int i = 0; i < fieldsCount; i++) {
                    field = heap.getField(reference, i);

                    if (field instanceof JReference) {
                        processReference((JReference) field);
                    } else {
                        continue;
                    }

                    heap.putField(reference, i, field);
                }
            } else {
                int arrayLength = HeapUtil.getArrayLength(reference).getValue();
                for (int i = 0; i < arrayLength; i++) {
                    field = heap.getArrayField(reference, i);

                    if (field instanceof JReference) {
                        processReference((JReference) field);
                    } else {
                        continue;
                    }

                    heap.putArrayField(reference, i, field);
                }
            }

            int newReference = heap.moveObjectToSecondHeapArray(reference);
            movedObjects.put(reference.getReference(), newReference);

            reference.setReference(newReference);
            reference.setMark(actualGCRunNumber);
        }
    }

    /**
     * add reference which wont be deleted when GC is started inside native method
     */

    public static void addNeededReference(JReference reference){
        neededReferences.add(reference);
    }

    public static void clearNeededReferences(){
        neededReferences.clear();
    }

}
