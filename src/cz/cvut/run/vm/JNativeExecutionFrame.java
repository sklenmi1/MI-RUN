package cz.cvut.run.vm;

import cz.cvut.run.instructions.JInstruction;
import cz.cvut.run.instructions.JNativeInstruction;
import cz.cvut.run.loader.JInstructionReader;
import cz.cvut.run.structure.method.JMethod;
import cz.cvut.run.util.Util;
import cz.cvut.run.vm.types.JType;

import java.util.Arrays;
import java.util.Stack;

/**
 * Created by Tadeas on 12.11.2015.
 */
public class JNativeExecutionFrame extends JExecutionFrame {

    JExecutionFrame callerFrame = null;

    public JNativeExecutionFrame(JExecutionFrame callerFrame, JMethod method) {
        super(method);
        this.isNative = true;
        this.callerFrame = callerFrame;
        int argumentCount = Util.getMethodArgumentCount(method.getDescriptor());
        this.localVariables = new JType[argumentCount + 1];
    }

//    public void setCallerFrame(JExecutionFrame callerFrame) {
//        this.callerFrame = callerFrame;
//    }

    @Override
    public JInstruction nextInstruction() {
        return new JNativeInstruction(callerFrame);
    }
}
