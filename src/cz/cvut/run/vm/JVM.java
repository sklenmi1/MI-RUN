package cz.cvut.run.vm;

import cz.cvut.run.instructions.JInstruction;
import cz.cvut.run.loader.JClassLoader;
import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.method.JMethod;
import cz.cvut.run.util.HeapUtil;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.JReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;


/**
 * Created by Tadeas on 12.11.2015.
 */
public class JVM {

    public static boolean FULL_LOG = false;

    private static JVM instance;

    private String classpathRootFolder;

    private JClassLoader loader;
    private Stack<JExecutionFrame> executionStack;
    private HashMap<String, JClass> loadedClass;
    private ArrayList<String> loadedClassIndexing;
    private int loadedClassActualPosition;

    private JVM() {
        this.instance = this;
        this.loader = new JClassLoader();
        this.executionStack = new Stack<>();
        this.loadedClass = new HashMap<>();
        this.loadedClassIndexing = new ArrayList<>();
        this.loadedClassActualPosition = 0;
    }

    public static JVM getInstance() {
        if (instance == null) {
            instance = new JVM();
        }
        return instance;
    }

    public void start(String filePath, String className, String[] arguments) {
        JClass mainClass = loader.loadClass(filePath, className);
        loadedClass.put(mainClass.getClassName(), mainClass);
        loadedClassIndexing.add(mainClass.getClassName());
        mainClass.setIndexInMethodArea(loadedClassActualPosition++);

        String fullPath = filePath + className;

        this.classpathRootFolder = fullPath.substring(0, fullPath.indexOf(mainClass.getClassName()));

        JMethod mainMethod = mainClass.findMethod("main", "([Ljava/lang/String;)V");

        if (mainMethod == null) {
            throw new RuntimeException("Not found main method!");
        }

        JExecutionFrame firstFrame = new JExecutionFrame(mainMethod);
        executionStack.push(firstFrame);

        JReference argsRef = getArgumentsArray(arguments);
        firstFrame.addToLocalVariables(0, argsRef);

        while (!executionStack.isEmpty()) {
            executionStack.peek().nextInstruction().execute(executionStack);
        }

        if (JVM.FULL_LOG) {
            System.out.println("Our JVM is going to finish!");
        }
    }

    private JReference getArgumentsArray(String[] arguments){
        JClass jClassString = JVM.getInstance().getClass("java/lang/String");

        Heap heap = Heap.getInstance();
        JReference array = heap.allocateArray(jClassString, arguments.length);
        JReference jvmString;

        for (int i = 0; i < arguments.length; i++) {
            jvmString = HeapUtil.getJReferenceFromNativeString(arguments[i]);
            heap.putArrayField(array, i, jvmString);
        }

        return array;
    }

    public JClass getClass(String name) {
        JClass jClass = loadedClass.get(name);
        if (jClass == null) {
            jClass = loader.loadClass(classpathRootFolder, name + ".class");
            loadedClass.put(jClass.getClassName(), jClass);
            loadedClassIndexing.add(jClass.getClassName());
            jClass.setIndexInMethodArea(loadedClassActualPosition++);
            if (!name.equals("sun/misc/VM")) {
                initClass(jClass);
            }

            if (JVM.FULL_LOG) {
                System.out.println((char) 27 + "[34m" + "Class " + name + " was successfully loaded." + (char) 27 + "[0m");
                System.out.println("------------------");
            }
        }

        if (jClass.getSuperClassIndex() > 0) {
            getClass(jClass.getSuperClassName());
        }

        return jClass;
    }

    public void initClass(JClass jClass) {
        /**
         * invoke clinit
         */
        JMethod initMethod = jClass.findMethod("<clinit>", "()V");
        if (initMethod != null && !jClass.isInitialized()) {
            JExecutionFrame initFrame = new JExecutionFrame(initMethod);
            executionStack.push(initFrame);

            /**
             * if executing of initFrame is finished than initializing of class is finished also
             */

            while (initFrame.hasNextInstruction()) {
                executionStack.peek().nextInstruction().execute(executionStack);
            }

            if (JVM.FULL_LOG) {
                System.out.println((char) 27 + "[34m" + "Class " + jClass.getClassName() + " was successfully initialized." + (char) 27 + "[0m");
                System.out.println("------------------");
            }

            jClass.setInitialized(true);
        }
    }

    public JClass getClass(int indexInMethodArea) {
        if ((indexInMethodArea == -1) || (indexInMethodArea > 100000)) {
            System.out.println("something goes probably wrong");
        }
        return getClass(loadedClassIndexing.get(indexInMethodArea));
    }

    public Stack<JExecutionFrame> getExecutionStack() {
        return executionStack;
    }

    public HashMap<String, JClass> getLoadedClass() {
        return loadedClass;
    }
}
