package cz.cvut.run.vm.exception;

/**
 * Created by michal.sklenar on 20/12/15.
 */
public class JVMException extends RuntimeException {
    public JVMException(String message) {
        super(message);
    }
}
