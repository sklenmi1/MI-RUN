package cz.cvut.run.vm;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.method.JMethod;
import cz.cvut.run.util.HeapUtil;
import cz.cvut.run.vm.heap.GarbageCollector;
import cz.cvut.run.vm.heap.Heap;
import cz.cvut.run.vm.types.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tadeas on 19.12.2015.
 */
public class JNativeInstructionSolver {

    public static JType solve(JExecutionFrame actualFrame) {
        JMethod method = actualFrame.getMethod();
        String name = method.getName();
        String descriptor = method.getDescriptor();

        boolean knownInstruction = false;
        JType result = null;

        if (name.equals("registerNatives") && (descriptor.equals("()V"))) {
            knownInstruction = true;
        }

        if (name.equals("nanoTime") && descriptor.equals("()J")) {
            result = nanoTime();
            knownInstruction = true;
        }

        if (name.equals("currentTimeMillis") && descriptor.equals("()J")) {
            result = currentTimeMillis();
            knownInstruction = true;
        }

        if (name.equals("identityHashCode") && descriptor.equals("(Ljava/lang/Object;)I")) {
            result = identityHashCode();
            knownInstruction = true;
        }

        if (name.equals("arraycopy") && descriptor.equals("(Ljava/lang/Object;ILjava/lang/Object;II)V")) {
            arrayCopy(actualFrame);
            knownInstruction = true;
        }

        if (name.equals("desiredAssertionStatus0") && descriptor.equals("(Ljava/lang/Class;)Z")) {
            result = desiredAssertionStatus();
            knownInstruction = true;
        }

        if (name.equals("getPrimitiveClass") && descriptor.equals("(Ljava/lang/String;)Ljava/lang/Class;")) {
            result = getPrimitiveClass();
            knownInstruction = true;
        }

        if (name.equals("floatToRawIntBits") && descriptor.equals("(F)I")) {
            result = floatToRawIntBits(actualFrame);
            knownInstruction = true;
        }

        if (name.equals("doubleToRawLongBits") && descriptor.equals("(D)J")) {
            result = doubleToRawLongBits(actualFrame);
            knownInstruction = true;
        }

        if (name.equals("initialize") && descriptor.equals("()V")) {
            knownInstruction = true;
        }

        if (name.equals("println") && descriptor.equals("(Ljava/lang/String;)V")) {
            println(actualFrame, "S");
            knownInstruction = true;
        }

        if (name.equals("println") && (descriptor.equals("(I)V") || descriptor.equals("(Z)V"))) {
            println(actualFrame, "I");
            knownInstruction = true;
        }

        if (name.equals("println") && descriptor.equals("(J)V")) {
            println(actualFrame, "J");
            knownInstruction = true;
        }

        if (name.equals("println") && descriptor.equals("(F)V")) {
            println(actualFrame, "F");
            knownInstruction = true;
        }

        if (name.equals("println") && descriptor.equals("(D)V")) {
            println(actualFrame, "D");
            knownInstruction = true;
        }

        if (name.equals("println") && descriptor.equals("(C)V")) {
            println(actualFrame, "C");
            knownInstruction = true;
        }

        if (name.equals("println") && descriptor.equals("()V")) {
            println(actualFrame, "E");
            knownInstruction = true;
        }

        if (name.equals("getClassLoader0") && descriptor.equals("()Ljava/lang/ClassLoader;")) {
            result = getClassLoader();
            knownInstruction = true;
        }

        if (name.equals("fillInStackTrace") && descriptor.equals("(I)Ljava/lang/Throwable;")) {
            result = fillInStackTrace(actualFrame);
            knownInstruction = true;
        }

        if (name.equals("readAllLines") && descriptor.equals("(Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/util/List;")) {
            result = readAllLines(actualFrame);
            knownInstruction = true;
        }

        if (name.equals("write") && descriptor.equals("(Ljava/lang/String;Ljava/lang/Iterable;Ljava/nio/charset/Charset;[Ljava/nio/file/OpenOption;)Ljava/nio/file/Path;")) {
            result = write(actualFrame);
            knownInstruction = true;
        }

        GarbageCollector.clearNeededReferences();

        if (!knownInstruction) {
            throw new RuntimeException("Unknown native method - " + name + " " + descriptor);
        }

        return result;
    }


    private static JLong nanoTime() {
        return new JLong(System.nanoTime());
    }

    private static JLong currentTimeMillis() {
        return new JLong(System.currentTimeMillis());
    }

    private static JInteger identityHashCode() {
        return new JInteger(0);
    }

    private static void arrayCopy(JExecutionFrame frame) {
        JReference srcArray = (JReference) frame.getFromLocalVariables(0);
        int srcPosition = ((JInteger) frame.getFromLocalVariables(1)).getValue();
        JReference desArray = (JReference) frame.getFromLocalVariables(2);
        int desPosition = ((JInteger) frame.getFromLocalVariables(3)).getValue();
        int length = ((JInteger) frame.getFromLocalVariables(4)).getValue();

        Heap heap = Heap.getInstance();

        for (int srcPos = srcPosition; srcPos < srcPosition + length; srcPos++) {
            heap.putArrayField(desArray, desPosition++, heap.getArrayField(srcArray, srcPos));
        }
    }

    private static JInteger desiredAssertionStatus() {
        return new JInteger(1);
    }

    private static JReference getPrimitiveClass() {
        return JReference.NULL;
    }

    private static JInteger floatToRawIntBits(JExecutionFrame frame) {
        return new JInteger((int) ((JFloat) frame.getFromLocalVariables(0)).getValue());
    }

    private static JLong doubleToRawLongBits(JExecutionFrame frame) {
        return new JLong((long) ((JDouble) frame.getFromLocalVariables(0)).getValue());
    }

    private static void println(JExecutionFrame frame, String type) {
        switch (type) {
            case "C":
                System.out.println((char) 27 + "[32m" + "# - " + (char)((JInteger)frame.getFromLocalVariables(1)).getValue()+ " " + (char) 27 + "[0m");
                return;
            case "I":
                System.out.println((char) 27 + "[32m" + "# - " + ((JInteger)frame.getFromLocalVariables(1)).getValue()+ " " + (char) 27 + "[0m");
                return;
            case "J":
                System.out.println((char) 27 + "[32m" + "# - " + ((JLong)frame.getFromLocalVariables(1)).getValue()+ " " + (char) 27 + "[0m");
                return;
            case "F":
                System.out.println((char) 27 + "[32m" + "# - " + ((JFloat)frame.getFromLocalVariables(1)).getValue()+ " " + (char) 27 + "[0m");
                return;
            case "D":
                System.out.println((char) 27 + "[32m" + "# - " + ((JDouble)frame.getFromLocalVariables(1)).getValue()+ " " + (char) 27 + "[0m");
                return;
            case "E":
                System.out.println((char) 27 + "[32m" + "# - " + (char) 27 + "[0m");
                return;
            default:
                JReference stringOurs = (JReference) frame.getFromLocalVariables(1);
                String nativeString = HeapUtil.getNativeStringFromJReference(stringOurs);
                System.out.println((char) 27 + "[32m" + "# - " + nativeString + " " + (char) 27 + "[0m");
                return;
        }
    }

    private static JReference getClassLoader() {
        return JReference.NULL;
    }

    private static JReference fillInStackTrace(JExecutionFrame frame) {
        return (JReference) frame.getFromLocalVariables(0);
    }

    private static JReference readAllLines(JExecutionFrame frame) {
        JReference path = (JReference) frame.getFromLocalVariables(0);

        String nativePath = HeapUtil.getNativeStringFromJReference(path);

        JClass jClassList = JVM.getInstance().getClass("java/util/JVMList");
        JClass jClassString = JVM.getInstance().getClass("java/lang/String");

        Heap heap = Heap.getInstance();

        JReference list = heap.allocateObject(jClassList);
        GarbageCollector.addNeededReference(list);


        List<String> nativeLines = null;

        try {
            nativeLines = Files.readAllLines(Paths.get(nativePath), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (nativeLines == null) {
            return JReference.NULL;
        }

        JReference jvmString;

        JReference array = heap.allocateArray(jClassString, nativeLines.size());
        GarbageCollector.addNeededReference(array);

        heap.putField(list, 0, array);

        for (int i = 0; i < nativeLines.size(); i++) {
            jvmString = HeapUtil.getJReferenceFromNativeString(nativeLines.get(i));
            heap.putArrayField(array, i, jvmString);
        }

        return list;
    }

    private static JReference write(JExecutionFrame frame) {
        JReference path = (JReference) frame.getFromLocalVariables(0);
        JReference jvmList = (JReference) frame.getFromLocalVariables(1);
        String nativePath = HeapUtil.getNativeStringFromJReference(path);

        Heap heap = Heap.getInstance();

        JReference stringArray = (JReference) heap.getField(jvmList, 0);

        JInteger size = HeapUtil.getArrayLength(stringArray);
        String nativeString;

        List<String> nativesStrings = new ArrayList<>();

        for (int i = 0; i < size.getValue(); i++) {
            nativeString = HeapUtil.getNativeStringFromJReference((JReference) heap.getArrayField(stringArray, i));
            nativesStrings.add(nativeString);
        }

        try {
            Files.write(Paths.get(nativePath), nativesStrings, Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return path;
    }


}
