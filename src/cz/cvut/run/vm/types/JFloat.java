package cz.cvut.run.vm.types;

import java.nio.ByteBuffer;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JFloat implements JType {
    private float value;

    public JFloat(float value) {
        this.value = value;
    }

    public static JType fromBytes(byte[] bytes) {
        return new JFloat(ByteBuffer.wrap(bytes).getFloat());
    }

    public float getValue() {
        return value;
    }

    public JFloat add(JFloat operand) {
        return new JFloat(this.getValue() + operand.getValue());
    }

    public JFloat sub(JFloat operand) {
        return new JFloat(this.getValue() - operand.getValue());
    }

    public JFloat multiply(JFloat operand) {
        return new JFloat(this.getValue() * operand.getValue());
    }

    public JFloat divide(JFloat operand) {
        return new JFloat(this.getValue() / operand.getValue());
    }

    public JFloat remainder(JFloat operand) {
        return new JFloat(this.getValue() % operand.getValue());
    }

    public JFloat negate() {
        this.value *= -1.0f;
        return this;
    }

    @Override
    public JFloat copy() {
        return new JFloat(this.value);
    }

    @Override
    public byte[] toBytes() {
        byte[] bytes = new byte[5];
        ByteBuffer.wrap(bytes).put(JType.JFloat).putFloat(value);
        return bytes;
    }

    @Override
    public byte[] toBytesWithoutTag() {
        byte[] bytes = new byte[4];
        ByteBuffer.wrap(bytes).putFloat(value);
        return bytes;
    }

    @Override
    public String toString() {
        return "JFloat{" +
                "value=" + value +
                '}';
    }

    @Override
    public int getBytesLength() {
        return 5;
    }
}
