package cz.cvut.run.vm.types;

import java.nio.ByteBuffer;

/**
 * Created by michal.sklenar on 14/11/15.
 */
public class JDouble implements JType {
    private double value;

    public JDouble(double value) {
        this.value = value;
    }

    public static JType fromBytes(byte[] bytes) {
        return new JDouble(ByteBuffer.wrap(bytes).getDouble());
    }

    public double getValue() {
        return value;
    }

    public JDouble add(JDouble operand) {
        return new JDouble(this.getValue() + operand.getValue());
    }

    public JDouble sub(JDouble operand) {
        return new JDouble(this.getValue() - operand.getValue());
    }

    public JDouble multiply(JDouble operand) {
        return new JDouble(this.getValue() * operand.getValue());
    }

    public JDouble divide(JDouble operand) {
        return new JDouble(this.getValue() / operand.getValue());
    }

    public JDouble remainder(JDouble operand) {
        return new JDouble(this.getValue() % operand.getValue());
    }

    public JDouble negate() {
        this.value *= -1.0;
        return this;
    }

    @Override
    public JDouble copy() {
        return new JDouble(this.value);
    }

    @Override
    public byte[] toBytes() {
        byte[] bytes = new byte[9];
        ByteBuffer.wrap(bytes).put(JType.JDouble).putDouble(value);
        return bytes;
    }

    @Override
    public byte[] toBytesWithoutTag() {
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).putDouble(value);
        return bytes;
    }

    @Override
    public String toString() {
        return "JDouble{" +
                "value=" + value +
                '}';
    }

    @Override
    public int getBytesLength() {
        return 5;
    }
}
