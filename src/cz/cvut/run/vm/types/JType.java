package cz.cvut.run.vm.types;

import java.io.Serializable;

/**
 * Created by Tadeas on 13.11.2015.
 */
public interface JType {
    byte JDouble = 0x01;
    byte JFloat = 0x02;
    byte JInteger = 0x03;
    byte JLong = 0x04;
    byte JReference = 0x05;

    JType copy();

    int getBytesLength();

    byte[] toBytes();

    byte[] toBytesWithoutTag();
}
