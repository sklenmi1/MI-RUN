package cz.cvut.run.vm.types;

import java.nio.ByteBuffer;

/**
 * Created by Tadeas on 18.11.2015.
 */
public class JReference implements JType {
    public static final int NULL_VAL = -1;
    public static JReference NULL = new JReference(-1);
    private int reference = NULL_VAL;
    private int mark;

    public JReference(Integer reference) {
        this.reference = reference;
//        if((reference.intValue() < -1) || (reference.intValue() >= 100000)){
//            System.out.println("should not be here");
//        }
    }

    public static JType fromBytes(byte[] bytes) {
        return new JInteger(ByteBuffer.wrap(bytes).getInt());
    }

    public int getReference() {
        return this.reference;
    }

    public void setReference(int reference) {
        this.reference = reference;
    }

    public boolean isNull() {
        return reference == -1;
    }

    public boolean isNotNull() {
        return !isNull();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JReference that = (JReference) o;

        return reference == that.reference;
    }

    @Override
    public JReference copy() {
        return new JReference(this.reference);
    }

    @Override
    public byte[] toBytes() {
        byte[] bytes = new byte[5];
        ByteBuffer.wrap(bytes).put(JType.JReference).putInt(reference);
        return bytes;
    }

    @Override
    public byte[] toBytesWithoutTag() {
        byte[] bytes = new byte[4];
        ByteBuffer.wrap(bytes).putInt(reference);
        return bytes;
    }

    @Override
    public int getBytesLength() {
        return 5;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}
