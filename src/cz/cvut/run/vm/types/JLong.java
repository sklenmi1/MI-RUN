package cz.cvut.run.vm.types;

import java.nio.ByteBuffer;

/**
 * Created by Tadeas on 13.11.2015.
 */
public class JLong implements JType {
    private long value;

    public JLong(long value) {
        this.value = value;
    }

    public static JType fromBytes(byte[] bytes) {
        return new JLong(ByteBuffer.wrap(bytes).getLong());
    }

    public long getValue() {
        return value;
    }

    public JLong add(JLong operand) {
        return new JLong(this.getValue() + operand.getValue());
    }

    public JLong sub(JLong operand) {
        return new JLong(this.getValue() - operand.getValue());
    }

    public JLong multiply(JLong operand) {
        return new JLong(this.getValue() * operand.getValue());
    }

    public JLong divide(JLong operand) {
        return new JLong(this.getValue() / operand.getValue());
    }

    public JLong remainder(JLong operand) {
        return new JLong(this.getValue() % operand.getValue());
    }

    public JLong bitwiseOr(JLong operand) {
        return new JLong(this.getValue() | operand.getValue());
    }

    public JLong bitwiseAnd(JLong operand) {
        return new JLong(this.getValue() & operand.getValue());
    }

    public JLong bitwiseXor(JLong operand) {
        return new JLong(this.getValue() ^ operand.getValue());
    }

    public JLong arithmeticShiftRight(JInteger operand) {
        return new JLong(this.getValue() >> operand.getValue());
    }

    public JLong arithmeticShiftLeft(JInteger operand) {
        return new JLong(this.getValue() << operand.getValue());
    }

    public JLong logicalShiftRight(JInteger operand) {
        return new JLong(this.getValue() >>> operand.getValue());
    }

    public JLong negate() {
        this.value *= -1L;
        return this;
    }

    @Override
    public JLong copy() {
        return new JLong(this.value);
    }

    @Override
    public byte[] toBytes() {
        byte[] bytes = new byte[9];
        ByteBuffer.wrap(bytes).put(JType.JLong).putLong(value);
        return bytes;
    }

    @Override
    public byte[] toBytesWithoutTag() {
        byte[] bytes = new byte[8];
        ByteBuffer.wrap(bytes).putLong(value);
        return bytes;
    }

    @Override
    public int getBytesLength() {
        return 9;
    }

    @Override
    public String toString() {
        return "JLong{" +
                "value=" + value +
                '}';
    }
}
