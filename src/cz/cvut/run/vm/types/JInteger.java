package cz.cvut.run.vm.types;

import java.nio.ByteBuffer;

/**
 * Created by Tadeas on 13.11.2015.
 */
public class JInteger implements JType {
    private int value;

    public JInteger(int value) {
        this.value = value;
    }

    public static JType fromBytes(byte[] bytes) {
        return new JInteger(ByteBuffer.wrap(bytes).getInt());
    }

    public int getValue() {
        return value;
    }

    public JInteger add(JInteger operand) {
        return new JInteger(this.getValue() + operand.getValue());
    }

    public JInteger sub(JInteger operand) {
        return new JInteger(this.getValue() - operand.getValue());
    }

    public JInteger multiply(JInteger operand) {
        return new JInteger(this.getValue() * operand.getValue());
    }

    public JInteger divide(JInteger operand) {
        return new JInteger(this.getValue() / operand.getValue());
    }

    public JInteger remainder(JInteger operand) {
        return new JInteger(this.getValue() % operand.getValue());
    }

    public JInteger bitwiseOr(JInteger operand) {
        return new JInteger(this.getValue() | operand.getValue());
    }

    public JInteger bitwiseAnd(JInteger operand) {
        return new JInteger(this.getValue() & operand.getValue());
    }

    public JInteger xor(JInteger operand) {
        return new JInteger(this.getValue() ^ operand.getValue());
    }

    public JInteger arithmeticShiftRight(JInteger operand) {
        return new JInteger(this.getValue() >> operand.getValue());
    }

    public JInteger arithmeticShiftLeft(JInteger operand) {
        return new JInteger(this.getValue() << operand.getValue());
    }

    public JInteger logicalShiftRight(JInteger operand) {
        return new JInteger(this.getValue() >>> operand.getValue());
    }

    public JInteger negate() {
        this.value *= -1;
        return this;
    }

    @Override
    public JInteger copy() {
        return new JInteger(this.value);
    }

    @Override
    public byte[] toBytes() {
        byte[] bytes = new byte[5];
        ByteBuffer.wrap(bytes).put(JType.JInteger).putInt(value);
        return bytes;
    }

    @Override
    public byte[] toBytesWithoutTag() {
        byte[] bytes = new byte[4];
        ByteBuffer.wrap(bytes).putInt(value);
        return bytes;
    }

    @Override
    public int getBytesLength() {
        return 5;
    }

    @Override
    public String toString() {
        return "JInteger{" +
                "value=" + value +
                '}';
    }
}
