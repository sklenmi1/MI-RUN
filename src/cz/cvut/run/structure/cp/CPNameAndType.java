package cz.cvut.run.structure.cp;

/**
 * Created by Sklico on 02.11.2015.
 */
public class CPNameAndType extends CPRecord {

    private int nameIndex;
    private int descriptorIndex;

    private String name;
    private String descriptor;

    public CPNameAndType() {
        super(CPRecord.CONSTANT_NameAndType);
    }

    public int getNameIndex() {
        return nameIndex;
    }

    public void setNameIndex(int nameIndex) {
        this.nameIndex = nameIndex;
    }

    public int getDescriptorIndex() {
        return descriptorIndex;
    }

    public void setDescriptorIndex(int descriptorIndex) {
        this.descriptorIndex = descriptorIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }
}
