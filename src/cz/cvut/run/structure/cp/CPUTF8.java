package cz.cvut.run.structure.cp;

import java.util.Arrays;

/**
 * Created by Tadeas on 02.11.2015.
 */
public class CPUTF8 extends CPRecord {

    private short length;
    //private byte bytes[];
    private String value;

    public CPUTF8() {
        super(CPRecord.CONSTANT_Utf8);
    }

    public byte[] getBytes() {
        return this.value.getBytes();
    }

    public void setBytes(byte[] bytes) {
        this.value = new String(bytes);
    }

    public short getLength() {
        return length;
    }

    public void setLength(short length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return value;
    }
}
