package cz.cvut.run.structure.cp;

/**
 * Created by Tadeas on 02.11.2015.
 */
public class CPInteger extends CPRecord {

    private int bytes;

    public CPInteger() {
        super(CPRecord.CONSTANT_Integer);
    }

    public int getBytes() {
        return bytes;
    }

    public void setBytes(int bytes) {
        this.bytes = bytes;
    }

    public int getValue() {
        return bytes;
    }


}
