package cz.cvut.run.structure.cp;

/**
 * Created by Tadeas on 02.11.2015.
 */
public class CPLong extends CPRecord {

    private int highBytes;
    private int lowBytes;
    private long value;

    public CPLong() {
        super(CPRecord.CONSTANT_Long);
    }

    public int getHighBytes() {
        return highBytes;
    }

    public void setHighBytes(int highBytes) {
        this.highBytes = highBytes;
    }

    public int getLowBytes() {
        return lowBytes;
    }

    public void setLowBytes(int lowBytes) {
        this.lowBytes = lowBytes;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
