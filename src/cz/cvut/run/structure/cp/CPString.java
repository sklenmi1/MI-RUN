package cz.cvut.run.structure.cp;

/**
 * Created by Tadeas on 02.11.2015.
 */
public class CPString extends CPRecord {

    private short stringIndex;
    //private CPUTF8 value;

    public CPString() {
        super(CPRecord.CONSTANT_String);
    }

    public short getStringIndex() {
        return stringIndex;
    }

    public void setStringIndex(short stringIndex) {
        this.stringIndex = stringIndex;
    }

    /*public void setValue(CPUTF8 CPvalue){
        this.value = CPvalue;
    }

    @Override
    public String toString() {
        return value.toString();
    }*/
}
