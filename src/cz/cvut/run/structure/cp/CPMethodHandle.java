package cz.cvut.run.structure.cp;

/**
 * Created by Sklico on 02.11.2015.
 */
public class CPMethodHandle extends CPRecord {

    private byte referenceKind;

    private short referenceIndex;

    public CPMethodHandle() {
        super(CPRecord.CONSTANT_MethodHandle);
    }

    public byte getReferenceKind() {
        return referenceKind;
    }

    public void setReferenceKind(byte referenceKind) {
        this.referenceKind = referenceKind;
    }

    public short getReferenceIndex() {
        return referenceIndex;
    }

    public void setReferenceIndex(short referenceIndex) {
        this.referenceIndex = referenceIndex;
    }
}
