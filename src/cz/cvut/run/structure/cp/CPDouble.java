package cz.cvut.run.structure.cp;

/**
 * Created by Sklico on 02.11.2015.
 */
public class CPDouble extends CPRecord {

    private int highBytes;
    private int lowBytes;
    private double value;

    public CPDouble() {
        super(CPRecord.CONSTANT_Double);
    }

    public int getHighBytes() {
        return highBytes;
    }

    public void setHighBytes(int highBytes) {
        this.highBytes = highBytes;
    }

    public int getLowBytes() {
        return lowBytes;
    }

    public void setLowBytes(int lowBytes) {
        this.lowBytes = lowBytes;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
