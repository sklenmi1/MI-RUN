package cz.cvut.run.structure.cp;

/**
 * Created by Tadeas on 02.11.2015.
 */
public class CPFieldRef extends CPRecord {

    private short classIndex;
    private short nameAndTypeIndex;

    public CPFieldRef() {
        super(CPRecord.CONSTANT_Fieldref);
    }

    public short getClassIndex() {
        return classIndex;
    }

    public void setClassIndex(short classIndex) {
        this.classIndex = classIndex;
    }

    public short getNameAndTypeIndex() {
        return nameAndTypeIndex;
    }

    public void setNameAndTypeIndex(short nameAndTypeIndex) {
        this.nameAndTypeIndex = nameAndTypeIndex;
    }
}
