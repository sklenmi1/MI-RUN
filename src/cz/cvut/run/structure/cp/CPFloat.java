package cz.cvut.run.structure.cp;

/**
 * Created by Tadeas on 02.11.2015.
 */
public class CPFloat extends CPRecord {

    private float value;

    public CPFloat() {
        super(CPRecord.CONSTANT_Float);
    }

    public int getBytes() {
        return Float.floatToIntBits(value);
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
