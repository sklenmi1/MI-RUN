package cz.cvut.run.structure.cp;

/**
 * Created by Sklico on 02.11.2015.
 */
public class CPMethodType extends CPRecord {

    private short descriptorIndex;

    public CPMethodType() {
        super(CPRecord.CONSTANT_MethodType);
    }

    public short getDescriptorIndex() {
        return descriptorIndex;
    }

    public void setDescriptorIndex(short descriptorIndex) {
        this.descriptorIndex = descriptorIndex;
    }
}
