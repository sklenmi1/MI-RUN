package cz.cvut.run.structure.cp;

/**
 * Created by Tadeas on 03.11.2015.
 */
public class CPForbidden extends CPRecord {
    public CPForbidden() {
        super(CPRecord.CONSTANT_Forbidden);
    }
}
