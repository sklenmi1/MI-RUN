package cz.cvut.run.structure.cp;

/**
 * Created by Sklico on 02.11.2015.
 */
public class CPInvokeDynamic extends CPRecord {

    private short bootstrapMethodAttrIndex;
    private short nameAndTypeIndex;

    public CPInvokeDynamic() {
        super(CPRecord.CONSTANT_InvokeDynamic);
    }

    public short getBootstrapMethodAttrIndex() {
        return bootstrapMethodAttrIndex;
    }

    public void setBootstrapMethodAttrIndex(short bootstrapMethodAttrIndex) {
        this.bootstrapMethodAttrIndex = bootstrapMethodAttrIndex;
    }

    public short getNameAndTypeIndex() {
        return nameAndTypeIndex;
    }

    public void setNameAndTypeIndex(short nameAndTypeIndex) {
        this.nameAndTypeIndex = nameAndTypeIndex;
    }
}
