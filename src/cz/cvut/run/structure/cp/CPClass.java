package cz.cvut.run.structure.cp;

/**
 * Created by Tadeas on 02.11.2015.
 */
public class CPClass extends CPRecord {

    private short nameIndex;

    public CPClass() {
        super(CPRecord.CONSTANT_Class);
    }

    public short getNameIndex() {
        return nameIndex;
    }

    public void setNameIndex(short nameIndex) {
        this.nameIndex = nameIndex;
    }
}
