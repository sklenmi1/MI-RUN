package cz.cvut.run.structure.method;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.attribute.JAttribute;
import cz.cvut.run.structure.attribute.JCodeAttribute;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tadeas on 02.11.2015.
 */
public class JMethod {
    public static final int ACC_PUBLIC = 0x0001;
    public static final int ACC_PRIVATE = 0x0002;
    public static final int ACC_PROTECTED = 0x0004;
    public static final int ACC_STATIC = 0x0008;
    public static final int ACC_FINAL = 0x0010;
    public static final int ACC_SYNCHRONIZED = 0x0020;
    public static final int ACC_BRIDGE = 0x0040;
    public static final int ACC_VARARGS = 0x0080;
    public static final int ACC_NATIVE = 0x0100;
    public static final int ACC_ABSTRACT = 0x0400;
    public static final int ACC_STRICT = 0x0800;
    public static final int ACC_SYNTHETIC = 0x1000;

    private boolean isPublic = false;
    private boolean isPrivate = false;
    private boolean isProtected = false;
    private boolean isStatic = false;
    private boolean isFinal = false;
    private boolean isSynchronized = false;
    private boolean isBridge = false;
    private boolean isVarargs = false;
    private boolean isNative = false;
    private boolean isAbstract = false;
    private boolean isStrict = false;
    private boolean isSynthetic = false;

    private short nameIndex;
    private String name;
    private short descriptorIndex;
    private String descriptor;
    private int attributesCount;
    private List<JAttribute> attributes;
    private JCodeAttribute codeAttribute;

    private JClass classReference;

    public JMethod(JClass classReference) {
        this.classReference = classReference;
        this.attributes = new ArrayList<>();
        attributesCount = 0;
    }

    public JClass getJClass() {
        return classReference;
    }

    public void addAttribute(JAttribute attributeObject) {
        if (attributeObject == null) {
            throw new RuntimeException("Trying to add null attribute object!");
        }
        attributes.add(attributesCount++, attributeObject);
    }

    public JAttribute getAttribute(int position) {
        return attributes.get(position);
    }

    public void setCodeAttribute(JCodeAttribute codeAttribute) {
        addAttribute(this.codeAttribute = codeAttribute);
    }

    public JCodeAttribute getCodeAttribute() {
        return this.codeAttribute;
    }


    public short getNameIndex() {
        return nameIndex;
    }

    public void setNameIndex(short nameIndex) {
        this.nameIndex = nameIndex;
        this.name = classReference.getFromConstantPool(nameIndex).toString();
    }

    public short getDescriptorIndex() {
        return descriptorIndex;
    }

    public void setDescriptorIndex(short descriptorIndex) {
        this.descriptorIndex = descriptorIndex;
        this.descriptor = classReference.getFromConstantPool(descriptorIndex).toString();
    }

    public String getDescriptor() {
        return descriptor;
    }

    public String getName() {
        return name;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public boolean isProtected() {
        return isProtected;
    }

    public void setIsProtected(boolean isProtected) {
        this.isProtected = isProtected;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setIsStatic(boolean isStatic) {
        this.isStatic = isStatic;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setIsFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public boolean isSynchronized() {
        return isSynchronized;
    }

    public void setIsSynchronized(boolean isSynchronized) {
        this.isSynchronized = isSynchronized;
    }

    public boolean isBridge() {
        return isBridge;
    }

    public void setIsBridge(boolean isBridge) {
        this.isBridge = isBridge;
    }

    public boolean isVarargs() {
        return isVarargs;
    }

    public void setIsVarargs(boolean isVarargs) {
        this.isVarargs = isVarargs;
    }

    public boolean isNative() {
        return isNative;
    }

    public void setIsNative(boolean isNative) {
        this.isNative = isNative;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public void setIsAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    public boolean isStrict() {
        return isStrict;
    }

    public void setIsStrict(boolean isStrict) {
        this.isStrict = isStrict;
    }

    public boolean isSynthetic() {
        return isSynthetic;
    }

    public void setIsSynthetic(boolean isSynthetic) {
        this.isSynthetic = isSynthetic;
    }


    @Override
    public String toString() {
        return "JMethod - name " + getJClass().getFromConstantPool(nameIndex) + " - descriptor " + getJClass().getFromConstantPool(descriptorIndex);
    }
}
