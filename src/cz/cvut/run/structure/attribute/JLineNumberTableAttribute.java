package cz.cvut.run.structure.attribute;

import cz.cvut.run.structure.linenumber.JLineNumberTableItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michal.sklenar on 20/12/15.
 */
public class JLineNumberTableAttribute extends JAttribute {

    public static final String NAME = "LineNumberTable";
    private short lineNumberTableLength;
    private List<JLineNumberTableItem> lineNumberTable;

    public JLineNumberTableAttribute() {
        super(NAME);
        this.lineNumberTable = new ArrayList<>();
    }

    public short getLineNumberTableLength() {
        return lineNumberTableLength;
    }

    public void setLineNumberTableLength(short lineNumberTableLength) {
        this.lineNumberTableLength = lineNumberTableLength;
    }

    public JLineNumberTableItem getFromLineNumberTable(int position) {
        return lineNumberTable.get(position);
    }

    public void addLineNumberTableItem(JLineNumberTableItem lnti, int index) {
        if (lnti == null) {
            throw new RuntimeException("Trying to add null linenumber item!");
        }

        lineNumberTable.add(index, lnti);
    }

    public List<JLineNumberTableItem> getLineNumberTable() {
        return lineNumberTable;
    }
}
