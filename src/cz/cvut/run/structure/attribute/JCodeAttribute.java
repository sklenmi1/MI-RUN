package cz.cvut.run.structure.attribute;

import cz.cvut.run.structure.exception.JExceptionTableItem;
import cz.cvut.run.structure.linenumber.JLineNumberTableItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tadeas on 02.11.2015.
 */
public class JCodeAttribute extends JAttribute {
    public static final String NAME = "Code";

    private short maxStack;
    private short maxLocals;
    private int codeLength;
    private byte[] instructions;
    private short exceptionTableLength;
    private List<JExceptionTableItem> exceptionTable;
    private int attributesCount;
    private List<JAttribute> attributes;

    public JCodeAttribute() {
        super(NAME);
        this.attributes = new ArrayList<>();
        attributesCount = 0;
        this.exceptionTable = new ArrayList<>();
    }

    public void addAttribute(JAttribute attributeObject) {
        if (attributeObject == null) {
            throw new RuntimeException("Trying to add null constant object!");
        }
        attributes.add(attributesCount++, attributeObject);
    }

    public JAttribute getAttribute(int position) {
        return attributes.get(position);
    }

    public List<JAttribute> getAttributes() {
        return attributes;
    }

    public short getMaxStack() {
        return maxStack;
    }

    public void setMaxStack(short maxStack) {
        this.maxStack = maxStack;
    }

    public short getMaxLocals() {
        return maxLocals;
    }

    public void setMaxLocals(short maxLocals) {
        this.maxLocals = maxLocals;
    }

    public int getCodeLength() {
        return codeLength;
    }

    public void setCodeLength(int codeLength) {
        this.codeLength = codeLength;
    }

    public byte[] getInstructions() {
        return instructions;
    }

    public void setInstructions(byte[] instructions) {
        this.instructions = instructions;
    }

    public short getExceptionTableLength() {
        return exceptionTableLength;
    }

    public void setExceptionTableLength(short exceptionTableLength) {
        this.exceptionTableLength = exceptionTableLength;
    }

    public JExceptionTableItem getFromExceptionTable(int position) {
        return exceptionTable.get(position);
    }

    public void addExceptionTableItem(JExceptionTableItem eti, int index) {
        if (eti == null) {
            throw new RuntimeException("Trying to add null exception item!");
        }

        exceptionTable.add(index, eti);
    }

    public List<JExceptionTableItem> getExceptionTable() {
        return exceptionTable;
    }


    public int getLineNumber(int programCounter) {
        for (JAttribute attr : getAttributes()) {
            if (attr.getName().equals(JLineNumberTableAttribute.NAME)) {
                for (int i = ((JLineNumberTableAttribute)attr).getLineNumberTableLength() - 1; i >= 0; i --) {
                    JLineNumberTableItem lnti = ((JLineNumberTableAttribute) attr).getFromLineNumberTable(i);
                    if (lnti.getStartPc() <= programCounter && lnti.getStartPc() <= getCodeLength()) {
                        return lnti.getLineNumber();
                    }
                }
            }
        }
        return -1;
    }
}
