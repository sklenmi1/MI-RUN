package cz.cvut.run.structure.attribute;

/**
 * Created by Tadeas on 02.11.2015.
 */
public class JConstantValueAttribute extends JAttribute {
    public static final String NAME = "ConstantValue";

    private short constantValueIndex;

    public JConstantValueAttribute() {
        super(NAME);
    }

    public short getConstantValueIndex() {
        return constantValueIndex;
    }

    public void setConstantValueIndex(short constantValueIndex) {
        this.constantValueIndex = constantValueIndex;
    }
}
