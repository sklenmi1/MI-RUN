package cz.cvut.run.structure.attribute;

/**
 * Created by Tadeas on 02.11.2015.
 */
public abstract class JAttribute {
    protected String name;

    public JAttribute(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
