package cz.cvut.run.structure.field;

import cz.cvut.run.structure.attribute.JAttribute;
import cz.cvut.run.vm.types.JType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tadeas on 02.11.2015.
 */
public class JField {
    public static final int ACC_PUBLIC = 0x0001;
    public static final int ACC_PRIVATE = 0x0002;
    public static final int ACC_PROTECTED = 0x0004;
    public static final int ACC_STATIC = 0x0008;
    public static final int ACC_FINAL = 0x0010;
    public static final int ACC_VOLATILE = 0x0040;
    public static final int ACC_TRANSIENT = 0x0080;
    public static final int ACC_SYNTHETIC = 0x1000;
    public static final int ACC_ENUM = 0x4000;


    private boolean isPublic = false;
    private boolean isPrivate = false;
    private boolean isProtected = false;
    private boolean isStatic = false;
    private boolean isFinal = false;
    private boolean isVolatile = false;
    private boolean isTransient = false;
    private boolean isSynthetic = false;
    private boolean isEnum = false;

    private short nameIndex;
    private short descriptorIndex;

    private String name;
    private String descriptor;

    private int attributesCount;
    private List<JAttribute> attributes;

    /**
     * static field can have assign value here
     */
    private JType value;

    public JField() {
        this.attributes = new ArrayList<>();
        attributesCount = 0;
        value = null;
    }

    public void addAttribute(JAttribute attributeObject) {
        if (attributeObject == null) {
            throw new RuntimeException("Trying to add null attribute object!");
        }
        attributes.add(attributesCount++, attributeObject);
    }

    public JAttribute getAttribute(int position) {
        return attributes.get(position);
    }


    public boolean isPublic() {
        return isPublic;
    }

    public void setIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public boolean isProtected() {
        return isProtected;
    }

    public void setIsProtected(boolean isProtected) {
        this.isProtected = isProtected;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setIsFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setIsStatic(boolean isStatic) {
        this.isStatic = isStatic;
    }

    public boolean isTransient() {
        return isTransient;
    }

    public void setIsTransient(boolean isTransient) {
        this.isTransient = isTransient;
    }

    public boolean isVolatile() {
        return isVolatile;
    }

    public void setIsVolatile(boolean isVolatile) {
        this.isVolatile = isVolatile;
    }

    public boolean isSynthetic() {
        return isSynthetic;
    }

    public void setIsSynthetic(boolean isSynthetic) {
        this.isSynthetic = isSynthetic;
    }

    public boolean isEnum() {
        return isEnum;
    }

    public void setIsEnum(boolean isEnum) {
        this.isEnum = isEnum;
    }

    public short getNameIndex() {
        return nameIndex;
    }

    public void setNameIndex(short nameIndex) {
        this.nameIndex = nameIndex;
    }

    public short getDescriptorIndex() {
        return descriptorIndex;
    }

    public void setDescriptorIndex(short descriptorIndex) {
        this.descriptorIndex = descriptorIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }


    public JType getValue() {
        return value;
    }

    public void setValue(JType value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JField jField = (JField) o;

        if (!name.equals(jField.name)) return false;
        return descriptor.equals(jField.descriptor);

    }
}
