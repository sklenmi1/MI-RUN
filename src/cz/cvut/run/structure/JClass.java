package cz.cvut.run.structure;

import cz.cvut.run.structure.attribute.JAttribute;
import cz.cvut.run.structure.cp.CPClass;
import cz.cvut.run.structure.cp.CPRecord;
import cz.cvut.run.structure.field.JField;
import cz.cvut.run.structure.method.JMethod;
import cz.cvut.run.util.Util;
import cz.cvut.run.vm.JVM;
import cz.cvut.run.vm.types.JType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tadeas on 01.11.2015.
 */
public class JClass {
    /*Class access flags according to jvms7.pdf*/

    public static final int ACC_PUBLIC = 0x0001;
    public static final int ACC_FINAL = 0x0010;
    public static final int ACC_SUPER = 0x0020;
    public static final int ACC_INTERFACE = 0x0200;
    public static final int ACC_ABSTRACT = 0x0400;
    public static final int ACC_SYNTHETIC = 0x1000;
    public static final int ACC_ANNOTATION = 0x2000;
    public static final int ACC_ENUM = 0x4000;

    private String className = null;
    private String superClassName = null;

    private int CPSize;
    private List<CPRecord> CP;
    private List<JField> fields;
    private int fieldsCount;
    private List<JMethod> methods;
    private int methodsCount;
    private List<JAttribute> attributes;
    private int attributesCount;
    private int interfaceIndexesCount;
    private List<Integer> interfaceIndexes;
    private short thisClassIndex;
    private short superClassIndex;
    private boolean isPublic = false;
    private boolean isFinal = false;
    private boolean isSuper = false;
    private boolean isInterface = false;
    private boolean isAbstract = false;
    private boolean isSynthetic = false;
    private boolean isAnnotation = false;
    private boolean isEnum = false;


    /**
     * contains index which can be used for searching loaded class
     */
    private int indexInMethodArea;
    private boolean initialized;

    public JClass() {
        CP = new ArrayList<>();
        CPSize = 0;
        interfaceIndexesCount = 0;
        interfaceIndexes = new ArrayList<>();
        fields = new ArrayList<>();
        fieldsCount = 0;
        methods = new ArrayList<>();
        methodsCount = 0;
        attributes = new ArrayList<>();
        attributesCount = 0;
        initialized = false;
    }

    public CPRecord getFromConstantPool(int position) {
        if(position == 0){
            System.out.println("should not be here!");
        }
        return CP.get(position - 1);
    }

    public void addToConstantPool(CPRecord constantObject) {
        if (constantObject == null) {
            throw new RuntimeException("Trying to add null constant object!");
        }
        CP.add(CPSize++, constantObject);
    }

    public List<JField> getFieldsList() {
        return fields;
    }

    public JField getField(int position) {
        return fields.get(position);
    }

    public JField getField(String name, String descriptor) {
//        int pos = getFieldIndex(name, descriptor);
//        if (pos == -1) {
//            return null;
//        }
//        return getField(pos);

        JClass actualClass = this;

        while (actualClass != null) {
            for (int i = 0; i < actualClass.fields.size(); i++) {
                if (actualClass.fields.get(i).getName().equals(name) && actualClass.fields.get(i).getDescriptor().equals(descriptor)) {
                    return actualClass.fields.get(i);
                }
            }
            actualClass = JVM.getInstance().getClass(actualClass.getSuperClassName());
        }

        throw new RuntimeException("Cannot find field - " + name + " , " + descriptor);
    }

    public JType getDefaultField(int fieldIndex) {
        JClass actualClass = this;
        JField field = null;
        while (actualClass != null) {
            if (fieldIndex >= actualClass.fields.size()) {
                fieldIndex -= actualClass.fields.size();
                actualClass = JVM.getInstance().getClass(actualClass.getSuperClassName());
            } else {
                field = actualClass.getField(fieldIndex);
                break;
            }
        }
        return Util.getDefaultValueForField(field.getDescriptor());
    }

    public void addField(JField field) {
        if (field == null) {
            throw new RuntimeException("Trying to add null field object!");
        }
        fields.add(fieldsCount++, field);
    }

    public int getFieldsCount() {

        if (superClassIndex > 0) {
            return JVM.getInstance().getClass(getSuperClassName()).getFieldsCount() + fieldsCount;
        }

        return fieldsCount;
    }

    public int getFieldIndex(String name, String descriptor) {
        for (int i = 0; i < fields.size(); i++) {
            if (fields.get(i).getName().equals(name) && fields.get(i).getDescriptor().equals(descriptor)) {
                return i;
            }
        }

        int index = fields.size();
        JClass actualClass = JVM.getInstance().getClass(getSuperClassName());

        while (actualClass != null) {
            for (int i = 0; i < actualClass.fields.size(); i++) {
                if (actualClass.fields.get(i).getName().equals(name) && actualClass.fields.get(i).getDescriptor().equals(descriptor)) {
                    return index;
                }
                index++;
            }
            actualClass = JVM.getInstance().getClass(actualClass.getSuperClassName());
        }

        return -1;
    }

    public JMethod getMethod(int position) {
        return methods.get(position);
    }

    public void addMethod(JMethod method) {
        if (method == null) {
            throw new RuntimeException("Trying to add null method object!");
        }
        methods.add(methodsCount++, method);
    }

    public JAttribute getAttribute(int position) {
        return attributes.get(position);
    }

    public void addAttribute(JAttribute attribute) {
        if (attribute == null) {
            throw new RuntimeException("Trying to add null attribute object!");
        }
        attributes.add(attributesCount++, attribute);
    }

    public int getInterfaceCPIndex(int position) {
        return interfaceIndexes.get(position);
    }

    public void addInterfaceCPIndex(int interfaceIndex) {
        if (interfaceIndex < 0) {
            throw new RuntimeException("Trying to add invalid index of interface");
        }
        interfaceIndexes.add(interfaceIndexesCount++, interfaceIndex);
    }

    public int getCPSize() {
        return this.CPSize;
    }

    public int getInterfaceIndexesCount() {
        return this.interfaceIndexesCount;
    }

    public short getThisClassIndex() {
        return this.thisClassIndex;
    }

    public void setThisClassIndex(short thisClassIndex) {
        this.thisClassIndex = thisClassIndex;
    }

    public short getSuperClassIndex() {
        return this.superClassIndex;
    }

    public void setSuperClassIndex(short superClassIndex) {
        this.superClassIndex = superClassIndex;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public void setIsFinal(boolean isFinal) {
        this.isFinal = isFinal;
    }

    public boolean isSuper() {
        return isSuper;
    }

    public void setIsSuper(boolean isSuper) {
        this.isSuper = isSuper;
    }

    public boolean isInterface() {
        return isInterface;
    }

    public void setIsInterface(boolean isInterface) {
        this.isInterface = isInterface;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public void setIsAbstract(boolean isAbstract) {
        this.isAbstract = isAbstract;
    }

    public boolean isSynthetic() {
        return isSynthetic;
    }

    public void setIsSynthetic(boolean isSynthetic) {
        this.isSynthetic = isSynthetic;
    }

    public boolean isAnnotation() {
        return isAnnotation;
    }

    public void setIsAnnotation(boolean isAnnotation) {
        this.isAnnotation = isAnnotation;
    }

    public boolean isEnum() {
        return isEnum;
    }

    public void setIsEnum(boolean isEnum) {
        this.isEnum = isEnum;
    }

    public int getIndexInMethodArea() {
        return indexInMethodArea;
    }

    public void setIndexInMethodArea(int indexInMethodArea) {
        this.indexInMethodArea = indexInMethodArea;
    }

    public JMethod findMethod(String name, String descriptor) {
        for (JMethod method : methods) {
            if (name.equals(method.getName()) && descriptor.equals(method.getDescriptor())) {
                return method;
            }
        }

        if (superClassIndex > 0) {
            return JVM.getInstance().getClass(getSuperClassName()).findMethod(name, descriptor);
        }
        return null;
    }

    public String getClassName() {
        if (className == null) {
            className = getFromConstantPool(((CPClass) getFromConstantPool(thisClassIndex)).getNameIndex()).toString();
        }
        return className;
    }

    public String getSuperClassName() {
        if (superClassName == null) {
            superClassName = getFromConstantPool(((CPClass) getFromConstantPool(superClassIndex)).getNameIndex()).toString();
        }
        return superClassName;
    }

    public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    @Override
    public String toString() {
        return "JClass{" +
                "CPSize=" + CPSize +
                '}';
    }

    public boolean isInstanceOf(JClass refClass) {
        if (!refClass.isInterface()) {
            return isSubClassOf(refClass);
        }
        if (superClassIndex > 0) {
            return JVM.getInstance().getClass(getSuperClassName()).isInstanceOf(refClass) || isImplementationOf(refClass);
        } else {
            return isImplementationOf(refClass);
        }
    }

    private boolean isImplementationOf(JClass refClass) {
        if (getClassName().equals(refClass.getClassName())) {
            return true;
        } else if (getInterfaceIndexesCount() == 0) {
            return false;
        }

        for (int i = 0; i < getInterfaceIndexesCount(); i++) {
            String ifaceName = getFromConstantPool(((CPClass) getFromConstantPool(interfaceIndexes.get(i))).getNameIndex()).toString();
            JClass ji = JVM.getInstance().getClass(ifaceName);
            if (ji.isImplementationOf(refClass)) {
                return true;
            }
        }
        return false;
    }

    private boolean isSubClassOf(JClass refClass) {
        if (getClassName().equals(refClass.getClassName())) {
            return  true;
        } else if (superClassIndex == 0) {
            return false;
        }
        return JVM.getInstance().getClass(getSuperClassName()).isSubClassOf(refClass);
    }


}
