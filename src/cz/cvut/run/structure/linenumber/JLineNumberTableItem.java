package cz.cvut.run.structure.linenumber;

/**
 * Created by michal.sklenar on 20/12/15.
 */
public class JLineNumberTableItem {

    private short startPc;
    private short lineNumber;

    public JLineNumberTableItem() {
    }

    public short getStartPc() {
        return startPc;
    }

    public void setStartPc(short startPc) {
        this.startPc = startPc;
    }

    public short getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(short lineNumber) {
        this.lineNumber = lineNumber;
    }
}
