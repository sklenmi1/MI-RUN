package cz.cvut.run.structure.exception;

import cz.cvut.run.structure.JClass;
import cz.cvut.run.structure.cp.CPClass;
import cz.cvut.run.vm.JVM;

/**
 * Created by michal.sklenar on 11/12/15.
 */
public class JExceptionTableItem {

    private short startPc;
    private short endPc;
    private short handlerPc;
    private short catchType;
    private JClass catchTypeClass;


    public JExceptionTableItem() {
    }

    public short getStartPc() {
        return startPc;
    }

    public void setStartPc(short startPc) {
        this.startPc = startPc;
    }

    public short getEndPc() {
        return endPc;
    }

    public void setEndPc(short endPc) {
        this.endPc = endPc;
    }

    public short getHandlerPc() {
        return handlerPc;
    }

    public void setHandlerPc(short handlerPc) {
        this.handlerPc = handlerPc;
    }

    public short getCatchType() {
        return catchType;
    }

    public void setCatchType(short catchType) {
        this.catchType = catchType;
    }

    public JClass getCatchTypeClass(JClass actualFrameClass) {
        if (getCatchType() == 0) {
            return null;
        }

        if (catchTypeClass == null) {
            String className = actualFrameClass.getFromConstantPool(((CPClass) actualFrameClass.getFromConstantPool(getCatchType())).getNameIndex()).toString();
            catchTypeClass = JVM.getInstance().getClass(className);
        }
        return this.catchTypeClass;
    }
}
