package java.util;

/**
 * Created by Tadeas on 03.01.2016.
 */
public class ArrayList<T extends Object> extends JVMList<T> {

    public ArrayList(int size) {
        super(new Object[size]);
    }
}
