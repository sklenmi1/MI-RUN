package java.util;

/**
 * Created by Tadeas on 02.01.2016.
 */
public class JVMList<T extends Object> implements List<T> {

    private Object[] values;

    public JVMList(Object[] values) {
        this.values = values;
    }

    @Override
    public int size() {
        return values.length;
    }

    @Override
    public boolean isEmpty() {
        if (values == null) {
            return true;
        }
        return values.length == 0;
    }

    @Override
    public boolean contains(Object o) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public Iterator<T> iterator() {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public T[] toArray() {
        return (T[]) values;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public boolean add(T t) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public boolean remove(Object o) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public void clear() {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public T get(int index) {
        return (T) values[index];
    }

    @Override
    public T set(int index, T element) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public void add(int index, T element) {
        if (index < 0 || index >= values.length) {
            throw new RuntimeException("OutOfBoundException!");
        }
        values[index] = element;
    }

    @Override
    public T remove(int index) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public int indexOf(Object o) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public ListIterator<T> listIterator() {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        throw new RuntimeException("Not implemented yet in JVMList! Should not be called at the moment!");
    }
}
