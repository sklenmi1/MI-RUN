package java.nio.file;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

/**
 * Created by Tadeas on 02.01.2016.
 */
public class Files {

    public native static List<String> readAllLines(String path, Charset charset) throws IOException;

    public static List<String> readAllLines(Path path, Charset charset) throws IOException {
        return readAllLines(path.toString(), charset);
    }

    public native static Path write(String path, Iterable<CharSequence> lines, Charset charset, OpenOption... options) throws IOException;

    public static Path write(Path path, Iterable<CharSequence> lines, Charset charset, OpenOption... options) throws IOException {
        return write(path.toString(), lines, charset, options);
    }

}
