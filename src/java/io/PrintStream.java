package java.io;

/**
 * Created by Tadeas on 19.12.2015.
 */
public class PrintStream {
    public native void println(String text);

    public void println(Object value) {
        println(value.toString());
    }
    public native void println(int value);
    public native void println(double value);
    public native void println(float value);
    public native void println(long value);
    public native void println(boolean value);
    public native void println(char value);
    public native void println();
}
