MI-RUN 2015/2016 (zimný semester)
====================

Semestrálna práca

Autori
---------------------

Tadeáš Sosín (sosintad)

Michal Sklenár (sklenmi1)

Adresárová štruktúra
---------------------

**/src** - zdrojové kódy VM
       
 - **./cz.cvut.run/dummy** - testovacie dáta používané pri vývoji VM

 - **./cz.cvut.run/example** - zdrojové kódy pre ukážkový príklad (SAT)
        
 - **./cz.cvut.run/instructions** - triedy implementujúce inštrukcie
       
 - **./cz.cvut.run/loader** - triedy zaisťujúce načítanie classfile a načítanie všetých implementovaných inštrukcií
     
 - **./cz.cvut.run/structure** - triedy implementujúce všetky podporované položky z constant poolu
       
 - **./cz.cvut.run/util** - triedy poskytujúce pomocné funkcie pre heapu a celú VM
       
 - **./cz.cvut.run/vm/types** - triedy implementujúce dátové typy podporované JVM 
        
 - **./cz.cvut.run/vm/heap** - triedy implementujúce heapu a garbage collector
       
 - **./cz.cvut.run/vm/exception/JVMException** - trieda reprezentujúca našu custom exception
       
 - **./cz.cvut.run/vm/JExecutionFrame.java** - trieda reprezentujúca execution frame
        
 - **./cz.cvut.run/vm/JNativeExecutionFrame.java** - trieda reprezentujúca natívny execution frame
       
 - **./cz.cvut.run/vm/JNativeInstructionSolver.java** - trieda riešiaca resolve natívnych metód
       
 - **./cz.cvut.run/vm/JVM.java** - trieda implementujúca základné správanie VM
       
 - **./cz.cvut.run/Main.java**  - trieda s main() metódou
       
**/data** - skompilované testovacie dáta a ukážkový príklad (SAT)

Spustenie programu
---------------------
Spustiteľný jar súbor sa nachádza v zložke final\_output pod názvom JVM.jar

**Spustenie**: **java -jar JVM.jar** *"argument_0" "argument_1"*

argument_0 - absolútna cesta k zložke obsahujúcej triedu s metódou main()

argument_1 - názov triedy obsahujúcej metódu main() bez prípony

argument_2 - absolutná cesta k vzorovému suboru SAT problému

Príklad: **java -jar JVM.jar** *"/Users/michal.sklenar/Documents/RUN/semestralka/data/cz/cvut/run/example/src/" "Main" "/Users/michal.sklenar/Documents/RUN/semestralka/data/cz/cvut/run/example/data/test_instance.cnf"*

Krátky popis obsahu semestrálnej práce
---------------------

Výsledkom tejto semestrálnej práce je program, ktorý dokáže načítať a vykonať inštrukcie skompilovaného kódu pre programovací jazyk Java 7 (pomocou java compileru).

Naša VM podporuje všetky základné aritmetické operácie i riadiace operácie. Pre dátový typ String podporujeme tieto operácie:

 - append
 - substring
 - indexOf
 - valueOf
 - contains

Okrem toho VM používa vlastný PrintStream a ArrayList, implementuje println pre rôzne dátové typy a implementuje niekoľko natívnych metód:

 - registerNatives
 - fillInStackTrace
 - nanoTime
 - currentTimeMillis
 - identityHashCode
 - arraycopy
 - getClassLoader

Podpora načítania(zápisu) zo(do) súboru, pomocí Files.readAllLines a Files.write.

Fungovanie našej VM je demonštrované na priloženom ukážkovom príklade (SAT), ktorého zdrojové kódy sa nachádzajú v zložke example.

Exceptions sú implementované tak, ako i v klasickej Jave.

Pamäť pre objekty je spravovaná garbage collectorom, ktorý je typu Baker.